" ============================================================================
" " File:        project.vim
" " Description: 
" " Author:      
" " Licence:     Vim licence
" " Website:     
" " Version:     0.0.1
" " Note:        
" "              
" " ============================================================================
"
scriptencoding utf-8

" If another plugin calls an autoloaded Ediyalam function on startup before the
" plugin/ediyalam.vim file got loaded, load it explicitly
if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif




" ***********
" * Worker  *
" ***********

let s:Worker = {}

function! s:Worker.New(name,root) dict
    let newobj = copy(self)
    call newobj._init(a:name,a:root)
    return newobj
endfunction
let s:local_SEQUENCE_worker = 1
function! s:Worker._init(name, root) dict
	let self.id            = s:local_SEQUENCE_worker
	let s:local_SEQUENCE_worker = s:local_SEQUENCE_worker + 1 
	let self.name          = a:name
	let self.task          = v:null
	let self.status        = 'ready'
endfunction

function! ediyalam#project#newWorker(name,root) 
	let worker = s:Worker.New(a:name,a:root)
	return worker
endfunction



" ***********
" * Project *
" ***********


let s:Project = {}

let s:PROJECT_DIR_NAME = '.ediyalam'
let s:PROJECT_FILE_NAME = s:PROJECT_DIR_NAME . '/project.json'

function! s:Project.New(name,root) dict
    let newProject = copy(self)
    call newProject._init(a:name,a:root)
    return newProject
endfunction

let s:local_SEQUENCE_project = 1

function s:Project._init(name, root) dict
	let self.__rt = {}
	let self.__p = {}

	let self.__p.name          = a:name

	let __rt = self.__rt

	let __rt.pluginData = {}
	let __rt.plugins = {}
	let __rt.id = s:local_SEQUENCE_project
	let s:local_SEQUENCE_project = s:local_SEQUENCE_project + 1 
	let __rt.path          = a:root
	let __rt.resourcesProvider = {}
	let __rt.resourcesProvider.plugin = {}
	let __rt.configurationProvider = {}
	let __rt.configurationProvider.plugin = {}
	let __rt.configurationProvider.pluginConfiguration = {}
	let __rt.open          = 0
	let __rt.dirty         = 0
endfunction

function s:Project.simpleView() dict
	let view = {}
	let view.name = self.__p.name
	let view.path = self.__rt.path
	let view.plugins = []
	return view
endfunction

function! s:Project.configurationPath() dict
	return self.__rt.path . '/' . s:PROJECT_DIR_NAME
endfunction

function! s:Project.pluginDataPath(pluginName) dict
	return self.configurationPath() . '/data/plugin/' . a:pluginName . '/plugin/' 
endfunction

function! s:Project.pluginConfigurationConfigurationPath(pluginName) dict
	return self.configurationPath() . '/config/plugin/' . a:pluginName . '/configuration/' 
endfunction

function! s:Project.pluginConfigurationPath(pluginName) dict
	return self.configurationPath() . '/config/plugin/' . a:pluginName . '/plugin/' 
endfunction

function! s:Project.projectConfigurationPath() dict
	return self.configurationPath() . '/config/project/' 
endfunction

function ediyalam#project#directoryContainsAProject(path)
	if filereadable(glob(a:path.'/'.s:PROJECT_FILE_NAME)) == 0 | return 0 | endif
	return 1
endfunction

function ediyalam#project#isAProjectOpen() abort
	if exists('g:ediyalam_project') == 0 | throw "g:ediyalam_project undefined" | endif
	if g:ediyalam_project is v:null
		return 0
	else
		return 1
	endif
endfunction

function s:initInternal(project)
	let a:project.__rt.configurationProvider.project = ediyalam#util#configuration#newConfigurationProvider(a:project.projectConfigurationPath())
	call ediyalam#plugin#plugin#initPluginsProjectInternal(a:project)
endfunction

function! ediyalam#project#createProjectInCWD(name) abort
	call ediyalam#project#createProject(a:name, getcwd())
endfunction
function! ediyalam#project#createProject(name,root) abort
	let project = ediyalam#project#newProject(name, root)
	call s:initInternal(project)
	call ediyalam#event#notifyProjectAfterOpen(project)
	let g:ediyalam_project = project
endfunction
function! ediyalam#project#closeProject(project) abort
	call ediyalam#event#notifyProjectBeforeClose(a:project)
	call ediyalam#project#saveProject(a:project)
	let g:ediyalam_project = v:null
endfunction
function! ediyalam#project#saveProject(project) abort
	call a:project.save()	
endfunction

function! ediyalam#project#newProject(name,root) 
	let absolutePath = ediyalam#util#system#makeSafePathAbsoluteFromCwd(a:root)
	let project = s:Project.New(a:name,absolutePath)
	return project
endfunction
function! s:Project.refundId()
	if s:local_SEQUENCE_project == self.id+0
		let s:local_SEQUENCE_project = self.id
	endif
endfunction


function! ediyalam#project#openProject(root) abort
	let project_file_path = a:root.'/'.s:PROJECT_FILE_NAME
	let file_raw = join(readfile(l:project_file_path),'')
	let project_raw = json_decode(l:file_raw)
	let project = ediyalam#project#newProject(project_raw.name, a:root)
	call s:initInternal(project)
	call project.load()
	call ediyalam#event#notifyProjectAfterOpen(project)
	let g:ediyalam_project = project
	return project
endfunction
function! ediyalam#project#openProjectInCWD() abort
	return ediyalam#project#openProject(getcwd())
endfunction



function! s:Project.existsOnDisk() dict
	return ediyalam#project#directoryContainsAProject(this.__rt.path)
endfunction

function! s:Project.prepareIDE() dict

	"echo 'Prepare IDE for '.self.name

	if g:ediyalam_on_project_focus_NERDTree_cd_into_root
		if exists(":NERDTreeTabs")
			"echo 'NERDTreeTabs('.(self.root).')'
			"silent call nerdtree#NERDTree(fnameescape(self.root))
		elseif exists(":NERDTree")
			"echo 'NERDTree('.(self.root).')'
			"silent execute 'NERDTree '.fnameescape(self.root)
		endif
	endif

	if g:ediyalam_on_project_focus_cd_into_root
		silent execute 'cd '.fnameescape(self.root)
		"echo 'CWD now is : '.self.root
	endif

endfunction

function! s:Project.save() dict
	let configurationProvider = self.__rt.configurationProvider.project
	call self.savePlugins()
	call s:saveWindows(configurationProvider)
	call s:saveBreakpoints(configurationProvider)
endfunction

function! s:Project.load() dict
	let configurationProvider = self.__rt.configurationProvider.project
	call self.loadPlugins()
	call s:loadWindows(configurationProvider)
	call s:loadBreakpoints(configurationProvider)
endfunction


function! s:Project.delete() dict
	call ediyalam#util#system#shellExecuteOrDie('rm -rf '.fnameescape(self.configurationPath()))
endfunction

function! s:Project.isOpen() dict
    return self.open != 0 ? 1 : 0
endfunction

function! s:Project.savePlugins() dict
	let configurationProvider = self.__rt.configurationProvider.project
	call configurationProvider.put('plugins', [])
endfunction

function! s:Project.loadPlugins() dict
	let configurationProvider = self.__rt.configurationProvider.project
	let plugins = configurationProvider.getOr('plugins', [])
	" FIXME handle bad configuration file
	for p in plugins
		let plugin = ediyalam#plugin#plugin#getBroker(p)
		if plugin is v:null
			call ediyalam#util#system#messageWarn ("Project '".self.__p.name."' uses unknown plugin '".p."'")
		else
			let configuration = v:null
			call self.applyPlugin(plugin, configuration)
		endif
	endfor
endfunction

function! s:Project.applyPlugin(plugin) dict
	" let configurationProvider = self.__rt.configurationProvider.pluginConfiguration
	let configuration = {} "configurationProvider.get('
	let self.__rt.plugins[a:plugin.name] = { 'plugin': a:plugin, 'configuration': configuration }
endfunction
function! s:Project.configurePlugin(pluginName) dict abort
	let configurationProvider = self.__rt.configurationProvider.project
	if ! has_key(self.__rt.plugins, a:pluginName) | throw "No such plugin '".a:pluginName."' for project '".self.__p.name."'" | endif
	let pluginRef = self.__rt.plugins[a:pluginName]

	for attribute in pluginRef.plugin.configuration.project.attributes
		let value = ediyalam#ui#plugin#getPath(attribute.label, '', {})
	endfor
endfunction

function s:getCurrentWindowConfiguration()
	let conf = {}
	let conf.b_name = buffer_name(bufnr('%'))
	let conf.layout = winsaveview()
	return conf
endfunction


function s:loadWindows(configurationProvider)
	let config = a:configurationProvider.getOr('windows', { 'current' : v:null, 'windows': [], 'layout': [] } )
	call ediyalam#util#layout#apply(config.layout)
endfunction

function s:addWindowConfig(window, windowsConfigs)
	let config = {}
	let config.nr = a:window.nr
	let config.id = a:window.id
	" let config.b_name = a:buffer.name
	" let winnr = bufwinnr(a:buffer.nr)
	" if winnr >= 0
		" let config.winnr = winnr
	" endif
	call add(a:windowsConfigs, config)
endfunction
function s:addBufferConfig(buffer, buffersConfigs)
	let config = {}
	let config.nr = a:buffer.nr
	let config.name = a:buffer.name
	let winnr = bufwinnr(a:buffer.nr)
	if winnr >= 0
		let config.winnr = winnr
	endif
	call add(a:buffersConfigs, config)
endfunction


function s:saveWindows(configurationProvider)
	let config = {}
	let config.current = buffer_name(winbufnr(0))
	let config.layout = winlayout()
	let buffers_configs = []
	call ediyalam#util#vimy#doWithBuffers(function('s:addBufferConfig'), buffers_configs, 1)
	let windows_configs = []
	call ediyalam#util#vimy#doWithWindows(function('s:addWindowConfig'), windows_configs)
	" windo call add(wins_configs, s:getCurrentWindowConfiguration())
	let config.buffers = buffers_configs
	let config.windows = windows_configs
	let config.layout = ediyalam#util#layout#get()
	call a:configurationProvider.put('windows', l:config)
endfunction

function s:saveBreakpoints(configurationProvider)
	let breakpoints = ediyalam#debug#debug#getBreakpoints()
	call a:configurationProvider.put('debug/breakpoints', l:breakpoints)
endfunction
function s:loadBreakpoints(configurationProvider)
	let breakpoints = a:configurationProvider.get('debug/breakpoints')
	call ediyalam#debug#debug#setBreakpoints(breakpoints)
endfunction

