
" If another plugin calls an autoloaded Ediyalam function on startup before the
" plugin/ediyalam.vim file got loaded, load it explicitly
if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif


let s:current_theme_index = 0
let s:current_airline_theme_index = 0

function! ediyalam#visual#Init()
	call ediyalam#visual#SetTextMarkers(g:ediyalam_visual_text_marker_level)
	call ediyalam#visual#SetTabWidth(g:ediyalam_visual_tab_width)
	let s:current_theme_index = g:ediyalam_visual_theme_airline_index-1
	call ediyalam#visual#ToggleColorTheme()
	let s:current_airline_theme_index = g:ediyalam_visual_theme_airline_index-1
	call ediyalam#visual#ToggleColorThemeAirline()
endfunction

function s:notifyInfoVisualMofication(message)
	call ediyalam#util#system#messageInfo(a:message)
endfunction
function s:notifyWarnVisualMofication(message)
	call ediyalam#util#system#messageWarn(a:message)
endfunction


function! ediyalam#visual#SetTextMarkers(level)
	let checked = [ '✔', '✖' ]
	let pen = '✎'

	let g:ediyalam_marker_icons_eol = [ '↲', '⏎' ]
	let g:ediyalam_marker_icon_eol = g:ediyalam_marker_icons_eol[0]
	let g:ediyalam_marker_icon_trail = '•'
	let g:ediyalam_marker_icons_tab = [ '▏', '▎', '▍' ]
	let g:ediyalam_marker_icon_tab = g:ediyalam_marker_icons_tab[1]
	"let g:ediyalam_marker_icon_tab = '¦'

	if a:level == 1
		execute 'set list listchars=eol:\ ,tab:\'.g:ediyalam_marker_icon_tab.'\ ,trail:\ ,extends:\ ,precedes:\ '
		let g:ediyalam_visual_text_marker_level=1
	call s:notifyInfoVisualMofication('Text marker level is : some')
	elseif a:level == 2
		execute 'set list listchars=eol:'.g:ediyalam_marker_icon_eol.',tab:\'.g:ediyalam_marker_icon_tab.'\ ,trail:'.g:ediyalam_marker_icon_trail.',extends:\ ,precedes:\ '
		let g:ediyalam_visual_text_marker_level=2
	call s:notifyInfoVisualMofication('Text marker level is : a lot')
	else
		execute 'set list listchars=eol:\ ,tab:\ \ ,trail:\ ,extends:\ ,precedes:\ '
		let g:ediyalam_visual_text_marker_level=0
	call s:notifyInfoVisualMofication('Text marker level is : none')
	endif
endfunction

function! ediyalam#visual#ToggleTextMarkers()
	let level = ediyalam#util#getNextLoopingInt(g:ediyalam_visual_text_marker_level,3)
	call ediyalam#visual#SetTextMarkers(level)
endfunction

function! ediyalam#visual#ToggleLineNumbers()
    set invnumber
    call ediyalam#util#system#message('Line Numbers are '.(&number?'On':'Off'))
endfunction
function! ediyalam#visual#SetGutter(active)
	if (a:active)
		if exists(':GitGutterEnable')
			GitGutterEnable
		endif
	else
		if exists(':GitGutterDisable')
			GitGutterDisable
		endif
	endif
endfunction
function! ediyalam#visual#ToggleGutter()
	if g:ediyalam_visual_gutter
		let g:ediyalam_visual_gutter=0
	else
		let g:ediyalam_visual_gutter=1
	endif
	call ediyalam#visual#SetGutter(g:ediyalam_visual_gutter)
	call ediyalam#util#system#message('Gutter is '.(g:ediyalam_visual_gutter?'On':'Off'))
endfunction


function! ediyalam#visual#ToggleColorTheme()
	if exists('g:ediyalam_themes') 
		\ && (len(g:ediyalam_themes)>0)
		let s:current_theme_index = ediyalam#util#getNextLoopingInt(s:current_theme_index,len(g:ediyalam_themes))
		let newTheme = g:ediyalam_themes[s:current_theme_index]
		execute 'colorscheme '.newTheme
		:redraw!
	call s:notifyInfoVisualMofication('Now using colors '.newTheme)
	else
		call s:notifyWarnVisualMofication('No color schemes defined')
	endif
endfunction

function! ediyalam#visual#ToggleColorFlavor()
    if &background == 'dark'
        set background=light
    else
        set background=dark 
    endif
	:redraw!
    call ediyalam#util#system#message('Now using '.&background.' background')
endfunction

function! ediyalam#visual#ToggleColorThemeAirline()
	"TODO: better way to check Airline presence
	if ! exists('g:airline_theme')
		call s:notifyWarnVisualMofication('Plugin "Airline" is not installed')
		return
	endif
	if exists('g:ediyalam_airline_themes') 
		\ && (len(g:ediyalam_airline_themes)>0)
		let s:current_airline_theme_index = ediyalam#util#getNextLoopingInt(s:current_airline_theme_index,len(g:ediyalam_airline_themes))
		let newTheme = g:ediyalam_airline_themes[s:current_airline_theme_index]
		execute 'AirlineTheme '.newTheme
		call s:notifyInfoVisualMofication('Now using Airline colors '.newTheme)
	else
		call s:notifyWarnVisualMofication('No Airlines color schemes defined')
	endif
endfunction



function! ediyalam#visual#SetTabWidth(width)
	let g:ediyalam_visual_tab_width = a:width
	execute 'set tabstop='.g:ediyalam_visual_tab_width.' softtabstop='.g:ediyalam_visual_tab_width.' shiftwidth='.g:ediyalam_visual_tab_width
endfunction
function! ediyalam#visual#ToggleTabWidth()
	let width = g:ediyalam_visual_tab_width
	if width == 1
		let width = 2
	elseif width == 2
		let width = 4
	elseif width == 4
		let width = 8
	else
		let width = 1
	endif
	call ediyalam#visual#SetTabWidth(width)
call s:notifyInfoVisualMofication('Tab width = '.g:ediyalam_visual_tab_width)
endfunction

function! ediyalam#visual#ToggleLineWrap()
	set invwrap
	call s:SetWrapCursorMouseWrapping(&wrap)
call s:notifyInfoVisualMofication('Line wrap is '.(&wrap?'On':'Off'))
endfunction

function! s:SetWrapCursorMouseWrapping(wrap_on)
	if a:wrap_on
		nnoremap <buffer> <Up> gk
		nnoremap <buffer> <Down> gj
		inoremap <buffer> <Up> <C-O>gk
		inoremap <buffer> <Down> <C-O>gj
		vnoremap <buffer> <Up> gk
		vnoremap <buffer> <Down> gj
	else
		setlocal nowrap
		nunmap <buffer> <Up>
		nunmap <buffer> <Down>
		iunmap <buffer> <Up>
		iunmap <buffer> <Down>
		vunmap <buffer> <Up>
		vunmap <buffer> <Down>
	endif
endfunction


function! ediyalam#visual#ToggleMouse()
	if &mouse == 'a'
		set mouse=
	call s:notifyInfoVisualMofication("Mouse is Off")
	else
		set mouse=a
	call s:notifyInfoVisualMofication("Mouse is On")
	endif
endfunction


