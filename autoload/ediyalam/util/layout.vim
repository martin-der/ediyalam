
" [Credit](https://vi.stackexchange.com/a/22545)

function ediyalam#util#layout#get() abort
	let layout_with_buffers = winlayout()
	" let s:resize_cmd = winrestcmd()
	call s:add_buf_to_layout(layout_with_buffers)
	return layout_with_buffers
endfunction

" add bufnr to leaf
function s:add_buf_to_layout(layout) abort
	if a:layout[0] ==# 'leaf'
		let buf = {'nr': winbufnr(a:layout[1])}
		let buf.name = bufname(buf.nr)
		call add(a:layout, buf)
	else
		for child_layout in a:layout[1]
			call s:add_buf_to_layout(child_layout)
		endfor
	endif
endfunction

function ediyalam#util#layout#apply(layout) abort
  " create clean window
  new
  wincmd o

  " recursively restore buffers
  call s:apply_layout(a:layout)

  " resize
  " exe s:resize_cmd
endfunction

function s:apply_layout(layout) abort

	if a:layout[0] ==# 'leaf'
		let buf = a:layout[2]
		if buf.name =~ '^NERD_tree'
		else
			exe printf('silent e %s', fnameescape(buf.name))
		endif
	else

		" split cols or rows, split n-1 times
		let split_method = a:layout[0] ==# 'col' ? 'rightbelow split' : 'rightbelow vsplit'
		let wins = [win_getid()]
		for child_layout in a:layout[1][1:]
		  exe split_method
		  let wins += [win_getid()]
		endfor

		" recursive into child windows
		for index in range(len(wins) )
		  call win_gotoid(wins[index])
		  call s:apply_layout(a:layout[1][index])
		endfor

	endif
endfunction

