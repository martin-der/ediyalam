

let s:ResourcesProvider = {}

function ediyalam#util#resource#newResourcesProvider(path)
	return s:ResourcesProvider.New(a:path)
endfunction

function s:ResourcesProvider.New(path) dict
    let provider = copy(self)
    call provider._init(a:path)
    return provider
endfunction

function s:ResourcesProvider._init(path) dict
	if a:path =~ '/$'
		let self.path = a:path
	else
		let self.path = a:path.'/'
	endif
endfunction

function s:validateRelativePath(path)
	if a:path =~ '^/'
		throw "Path must not start with '/'"
	endif
	if a:path =~ '\(^\|/\)\.\.\(/\|$\)'
		throw "Path must not contains '..' folder."
	endif
	if a:path =~ '\n'
		throw "Path must not contains '\n'."
	endif
endfunction

function s:ResourcesProvider.resolve(path) dict
	if a:path == '.'
		return self.path
	else
		return self.path . a:path
	endif
	g:NERDTreeFileNode.GetSelected()
endfunction

function s:ResourcesProvider.exists(path) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(a:path)
	return filereadable(l:finalPath)
endfunction

function s:ResourcesProvider.mkParentDirs(path) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(ediyalam#util#system#getParentDirectory(a:path))
	if finalPath != '.'
		call ediyalam#util#system#mkdir(l:finalPath)
	endif
endfunction
function s:ResourcesProvider.mkDirs(path) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(a:path)
	call ediyalam#util#system#mkdir(l:finalPath)
endfunction

function s:ResourcesProvider.getDictFromJson(path) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(a:path)
	let raw_content = readfile(l:finalPath)
	let content = join(l:raw_content,'')
	return json_decode(l:content)
endfunction
function s:ResourcesProvider.putDictAsJson(path, content) dict
	let string_content = json_encode(a:content)
	call self.put(a:path, l:string_content)
endfunction

function s:ResourcesProvider.get(path) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(a:path)
	let raw_content = readfile(l:finalPath)
	return join(l:raw_content,"\n")
endfunction
function s:ResourcesProvider.put(path, content) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(a:path)
	exec 'redir! > '.fnameescape(l:finalPath)
	try
		silent echon a:content
	finally
		redir END
	endtry
endfunction
function s:ResourcesProvider.list(path) dict
	call s:validateRelativePath(a:path)
	let finalPath = self.resolve(a:path)
	return ediyalam#util#system#ls(l:finalPath)
endfunction


