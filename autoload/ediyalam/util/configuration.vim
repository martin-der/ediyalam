

let s:ConfigurationProvider = {}

function ediyalam#util#configuration#newConfigurationProvider(path)
	return s:ConfigurationProvider.New(a:path)
endfunction

function s:ConfigurationProvider.New(path) dict
    let provider = copy(self)
    call provider._init(a:path)
    return provider
endfunction

function s:ConfigurationProvider._init(path) dict
	let self.resourcesProvider = ediyalam#util#resource#newResourcesProvider(a:path)
endfunction

function s:validatePath(path)
	if a:path =~ '^/\|/$'
		throw "Invalid name '" . a:path . "' : Path cannot start or end with '/'"
	endif
	if a:path =~ '^\.\|\.$'
		throw "Invalid name '" . a:path . "' : Path cannot start or end with '.'"
	endif
	if a:path =~ '[^a-zA-Z01-9\._\-/\$#]'
		throw "Invalid path '" . a:path . "' : Must contains only a-zA-Z0-9._-/$#"
	endif
endfunction

function s:ConfigurationProvider.getOr(path, defaultValue) dict
	let conf_file = a:path . '.json'
	call s:validatePath(l:conf_file)
	if self.resourcesProvider.exists(l:conf_file)
		return self.resourcesProvider.getDictFromJson(l:conf_file)
	else
		return a:defaultValue
endfunction
function s:ConfigurationProvider.get(path) dict
	let conf_file = a:path . '.json'
	call s:validatePath(l:conf_file)
	return self.resourcesProvider.getDictFromJson(l:conf_file)
endfunction

function s:ConfigurationProvider.put(path, content) dict
	let conf_file = a:path . '.json'
	call s:validatePath(l:conf_file)
	call self.resourcesProvider.mkParentDirs(l:conf_file)
	call self.resourcesProvider.putDictAsJson(l:conf_file, a:content)
endfunction
