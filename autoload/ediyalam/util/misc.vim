

let s:hi_popup=0


" let s:myfunction = luaeval('require("./ui").fancy_floating_markdown')

function! ediyalam#util#misc#OpenPopup()
	" call s:myfunction()
	" lua ediyalam#
	let buf = nvim_create_buf(v:false, v:true)
	call nvim_buf_set_lines(buf, 0, -1, v:true, g:ediyalam_icon_text)
	let opts = {'relative': 'cursor', 'width': 10, 'height': 2, 'col': 0,
		\ 'row': 1, 'anchor': 'NW', 'style': 'minimal'}
	let s:hi_popup= nvim_open_win(buf, 0, opts)
	" optional: change highlight, otherwise Pmenu is used
	call nvim_win_set_option(s:hi_popup, 'winhl', 'Normal:MyHighlight')
endfunction
function! ediyalam#util#misc#closePopup()
	call nvim_win_close(s:hi_popup,1)
endfunction

let s:Property = {}
function! s:Property.New(key,value) dict
    let newobj = copy(self)
	call newobj._init(a:key,a:value)
    return newobj
endfunction

function! s:Property._init(key, value) dict
	let self.key       = a:key
	let self.value      = a:value
endfunction

function! ediyalam#util#misc#newProperty(key,value) 
	let property = s:Property.New(a:key,a:value)
	return property
endfunction

" return property with 'key' == '' if 'line' was invalid
function! ediyalam#util#misc#LineToProperty(line)
	let property = ediyalam#util#misc#newProperty('','')

	if call ediyalam#util#misc#LineNotEmptyNotComment(a:line) == 0
		return property
	endif

	let pos = stridx(a:line, "=")
	if pos < 1 | return property | endif
	let key = strpart(a:line,0,pos)
	let key = ediyalam#util#string#trim(key)
	let value = strpart(a:line,pos+1)
	let property.key = key
	let property.value = value
	return property
endfunction



function! ediyalam#util#misc#LineNotEmptyNotComment(line)
	if a:line =~ '^[\t ]*#.*' | return 0 | endif
	if a:line =~ '^[\t ]$' | return 0 | endif
	return 1
endfunction


