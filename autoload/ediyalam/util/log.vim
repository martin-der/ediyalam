
if exists(':Ediyalam') == 0
	runtime plugin/ediyalam.vim
endif

if exists('g:ediyalam_logger_done')
	finish
end
let g:ediyalam_logger_done = 1

function! s:default_plugin_log_debug(log, level, text)
	call ediyalam#util#log#debug(a:log.prefix . a:text)
endfunction
function! s:default_plugin_log_info(log, level, text)
	call ediyalam#util#log#info(a:log.prefix . a:text)
endfunction
function! s:default_plugin_log_warn(log, level, text)
	call ediyalam#util#log#warn(a:log.prefix . a:text)
endfunction
function! s:default_plugin_log_error(log, level, text)
	call ediyalam#util#log#error(a:log.prefix . a:text)
endfunction

let s:default_fct_ref = {
	\ 'fct_debug': function('s:default_plugin_log_debug'),
	\ 'fct_info': function('s:default_plugin_log_info'),
	\ 'fct_warn': function('s:default_plugin_log_warn'),
	\ 'fct_error': function('s:default_plugin_log_error')
\}
function ediyalam#util#log#newPluginLog(plugin, level)
	let l:log = loggev#log#newPlugin('['.a:plugin.'] ', a:level)

	let l:log.__log.debug = s:default_fct_ref.fct_debug
	let l:log.__log.info = s:default_fct_ref.fct_info
	let l:log.__log.warn = s:default_fct_ref.fct_warn
	let l:log.__log.error = s:default_fct_ref.fct_error

	return l:log
endfunction



function ediyalam#util#log#debug(text)
	echohl EdiyalamMessageDebug | echom a:text | echohl None
endfunction
function ediyalam#util#log#log(text)
	call ediyalam#util#log#ok(a:text)
endfunction
function ediyalam#util#log#done(text)
	echohl EdiyalamMessageDone | echom a:text | echohl None
endfunction
function ediyalam#util#log#notice(text)
	echohl EdiyalamMessageNotice | echom a:text | echohl None
endfunction
function ediyalam#util#log#ok(text)
	echohl EdiyalamMessageOk | echom a:text | echohl None
endfunction
function ediyalam#util#log#info(text)
	echohl EdiyalamMessageInfo | echom a:text | echohl None
endfunction
function ediyalam#util#log#warn(text)
	echohl EdiyalamMessageWarn | echom a:text | echohl None
endfunction
function ediyalam#util#log#error(text)
	echohl EdiyalamMessageError | echom a:text | echohl None
endfunction



