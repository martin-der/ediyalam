
" [Credit](https://stackoverflow.com/a/6271254)
function ediyalam#util#vimy#getVisualSelection()
    let [line_start, column_start] = getpos("'<")[1:2]
    let [line_end, column_end] = getpos("'>")[1:2]
    let lines = getline(line_start, line_end)
    if len(lines) == 0
        return ''
    endif
    let lines[-1] = lines[-1][: column_end - (&selection == 'inclusive' ? 1 : 2)]
    let lines[0] = lines[0][column_start - 1:]
    return join(lines, "\n")
endfunction

function! ediyalam#util#vimy#doWithWindows(consumer,data)
	let wincount = winnr('$')
	let nr=1
	while nr <= wincount
		call a:consumer({'nr':nr, 'id': win_getid(nr)}, a:data)
		let nr += 1
	endwhile
endfunction


function ediyalam#util#vimy#doWithBuffers(consumer,data,unlisted) abort
    let l:bufnrs = range(1, bufnr('$'))
    call filter(l:bufnrs, a:unlisted ? {_, v -> bufexists(v)} : {_, v -> buflisted(v)})
	for nr in l:bufnrs
		call a:consumer({'nr':nr, 'name':bufname(nr), 'listed':buflisted(nr)}, a:data)
	endfor
    " let bufnames = copy(bufnrs)
        " \ ->map({_, v -> bufname(v)->fnamemodify(':t')})
    " let uniq_flags = copy(bufnames)->map({_, v -> count(bufnames, v) == 1})
    " let items = map(bufnrs, {i, v -> #{
        " \ bufnr: v,
        " \ text: s:gettext(v, uniq_flags[i]),
        " \ }})
    " call setloclist(0, [], ' ', #{
        " \ items: items,
        " \ title: 'ls' .. (a:bang ? '!' : ''),
        " \ quickfixtextfunc: 's:quickfixtextfunc',
        " \ })
    " lopen
    " nmap <buffer><nowait><expr><silent> <cr> <sid>Cr()
endfunction
