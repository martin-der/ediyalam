
" Set to 'truthy' when some sort of modern font can be used
let s:can_fancy_font = has('multi_byte') && has('unix') && 
                        \ &encoding == 'utf-8' &&
                        \ (empty(&termencoding) || &termencoding == 'utf-8')


function ediyalam#util#system#canFancyFont()
	return s:can_fancy_font
endfunction

function ediyalam#util#system#getParentDirectory(path)
	let cleaned = substitute(a:path, '/*$', '', 'g') 
	return fnamemodify( l:cleaned, ':h')
endfunction

function ediyalam#util#system#ls(path)
	return split ( glob(a:path . '/*'), '\n' )
endfunction
function ediyalam#util#system#mkdir(path)
	call mkdir ( a:path, 'p' )
endfunction

function! ediyalam#util#system#makePathAbsoluteFromCwd(relative)
	let cleaned = getcwd().'/'.a:relative
	if cleaned =~ '/\.$'
		let cleaned = cleaned[0:strlen(cleaned)-3]
	elseif cleaned =~ '/$'
		let cleaned = cleaned[0:strlen(cleaned)-2]
	endif
	return cleaned
endfunction
function! ediyalam#util#system#makeSafePathAbsoluteFromCwd(path)
	if a:path =~ '^/'
		return a:path
	endif
	if a:path =~ '^\~'
		return a:path
	endif
	return ediyalam#util#system#makePathAbsoluteFromCwd(a:path)
endfunction



function! ediyalam#util#system#findVimDirectory()
	if !empty(&rtp)
		return split(&rtp, ',')[0]
	endif
	throw 'Cannot find Vim directory'
endfunction

" @return v:shell_error
function! ediyalam#util#system#shellExecute(commmand)
	silent execute '!'.a:commmand
	return v:shell_error
endfunction
" @throws exception if command failed
function! ediyalam#util#system#shellExecuteOrDie(commmand)
	let result = ediyalam#util#system#shellExecute(a:commmand)
	if result != 0
		throw 'Execution of "'.a:commmand.'" failed with return code '.result
	endif
endfunction
" @throws exception if command failed
function! ediyalam#util#system#shellExecuteAndGetStdOut(commmand)
	exec 'redir! > '.fnameescape((l:path).'/'.s:PROJECT_FILE_NAME)
	try
		silent echo json_encode(content)
	finally
		redir END
	endtry
	silent execute '!'.a:commmand
	return v:shell_error
endfunction

" some message function 

function! ediyalam#util#system#message(text)
	echohl Normal | echo g:ediyalam_message_prefix | echohl EdiyalamMessage | echon a:text | echohl None
endfunction
function! ediyalam#util#system#messageNotice(text)
 	echohl Normal | echo g:ediyalam_message_prefix | echohl EdiyalamMessageNotice | echon a:text | echohl None
endfunction
function! ediyalam#util#system#messageDone(text)
 	echohl Normal | echo g:ediyalam_message_prefix | echohl EdiyalamMessageDone | echon a:text | echohl None
endfunction
function! ediyalam#util#system#messageInfo(text)
 	echohl Normal | echo g:ediyalam_message_prefix | echohl EdiyalamMessageOk | echon a:text | echohl None
endfunction
function! ediyalam#util#system#messageWarn(text)
 	echohl Normal | echo g:ediyalam_message_prefix | echohl EdiyalamMessageWarn | echon a:text | echohl None
endfunction
function! ediyalam#util#system#messageError(text)
	echohl Normal | echo g:ediyalam_message_prefix | echohl EdiyalamMessageError | echon a:text | echohl None
endfunction


