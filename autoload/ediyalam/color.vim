

" If another plugin calls an autoloaded Ediyalam function on startup before the
" plugin/ediyalam.vim file got loaded, load it explicitly
if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif

let s:enable_italic = 1

let s:hlg_prefix = 'Ediyalam'


function! s:HIstrIfNeeded(unprefixed_group, str)
	if ! hlID(s:hlg_prefix.a:unprefixed_group)
		call s:HIstr(a:unprefixed_group, a:str)
	endif
endfunction

function! s:HIstr(unprefixed_group, str)
	let params = split(a:str,'[\t ]\+')
	try
		let fg = params[0]
		let bg = params[1]
		let fx = len(params)>2?params[2]:'-'
	catch
		throw 'Invalid highlight string parameter : "'.a:str."'"
	endtry
	call hih#doHighlight(s:hlg_prefix.a:unprefixed_group, fg, bg, fx)
endfunction


function! ediyalam#color#createHighlights()
	call s:HIstrIfNeeded('MessageDebug', g:ediyalam_highlight_message_debug)
	call s:HIstrIfNeeded('Message', g:ediyalam_highlight_message)
	call s:HIstrIfNeeded('MessageOk', g:ediyalam_highlight_message_ok)
	call s:HIstrIfNeeded('MessageDone', g:ediyalam_highlight_message_done)
	call s:HIstrIfNeeded('MessageNotice', g:ediyalam_highlight_message_notice)
	call s:HIstrIfNeeded('MessageInfo', g:ediyalam_highlight_message_info) 
	call s:HIstrIfNeeded('MessageWarn', g:ediyalam_highlight_message_warn)
	call s:HIstrIfNeeded('MessageError', g:ediyalam_highlight_message_error)
endfunction


