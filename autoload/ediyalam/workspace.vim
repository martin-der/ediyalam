scriptencoding utf-8

if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif


let s:WORKSPACE_DIR = $HOME.'/.ediyalam-workspace'
let s:PROJECTS_LIST_FILE_NAME = s:WORKSPACE_DIR.'/projects'

let s:initCallBacks = []

function ediyalam#workspace#init()
	call s:init()
endfunction

function s:init()
	for Cb in s:initCallBacks
		call Cb()
	endfor
	if g:ediyalam_on_enter_load_workspace != 0
		let current_directory = getcwd()
		if ediyalam#project#directoryContainsAProject(current_directory)
			call ediyalam#projectOpen(current_directory)
			let name = g:ediyalam_project.__p.name
			call ediyalam#util#log#notice("Opened '" . name . "' from current working directory")
		endif
	endif
endfunction

function! ediyalam#workspace#leave()
	if ! ( g:ediyalam_project is v:null )
		call ediyalam#project#closeProject(g:ediyalam_project)
	endif
endfunction

function ediyalam#workspace#registerInit(cb)
	call add(s:initCallBacks, a:cb)
endfunction
