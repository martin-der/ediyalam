scriptencoding utf-8

if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif


let s:WORKSPACE_DIR = $HOME.'/.ediyalam-workspace'
let s:PROJECTS_LIST_FILE_NAME = s:WORKSPACE_DIR.'/projects'

let s:initPlugins = []

function ediyalam#workspace#init()
	call s:init()
endfunction

function s:init()
	let ediyalam_data = {}
	for p in s:initPlugins
		let Cb = p.cb
		try
			call Cb(ediyalam_data, p.plugin)
		catch
			call ediyalam#util#log#error("Initialisation of plugin '" . p.plugin.name . "' failed : ".v:exception . " | " . v:throwpoint)
		endtry
	endfor

	let no_file_provide_in_command_line = argc() == 0
	if g:ediyalam_on_enter_load_workspace
		if no_file_provide_in_command_line
			let current_directory = getcwd()
			if ediyalam#project#directoryContainsAProject(current_directory)
				call ediyalam#projectOpen(current_directory)
				let name = g:ediyalam_project.__p.name
				call ediyalam#util#log#notice("Opened '" . name . "' from current working directory")
			endif
		endif
	endif
endfunction

function! ediyalam#workspace#leave()
	if ! ( g:ediyalam_project is v:null )
		call ediyalam#project#closeProject(g:ediyalam_project)
	endif
endfunction

function ediyalam#workspace#registerInit(plugin, cb)
	call add(s:initPlugins, { 'plugin': a:plugin, 'cb': a:cb })
endfunction
