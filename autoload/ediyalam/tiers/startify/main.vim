

function s:listProjects()
	let projects = ediyalam#listProjects(0)
	let results = []
	for p in projects
		let result = {}
		let result.cmd = "call ediyalam#projectOpenAndAddToList('".p.path."')"
		let result.line = p.name
		" let result.line .= ' ('. p.path . ')'		
		call add(results, result)
	endfor
	return results 
endfunction

function ediyalam#tiers#startify#main#Init()
	if get(g:, 'ediyalam_startify_show_projects', 0)
		let extra_elements = [{ 'header': ['   Projects'], 'type': function('s:listProjects') }]
		if exists('g:startify_lists')
			if get(g:, 'ediyalam_startify_show_projects_at_the_top', 1)
				let g:startify_lists = l:extra_elements + g:startify_lists
			else
				let g:startify_lists = g:startify_lists + l:extra_elements
			endif
		else
			let g:startify_lists = l:extra_elements
		endif
	endif
endfunction

