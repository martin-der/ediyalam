
let s:inlined_menus = 1

let s:STRING__in_parent_menu__create_project = 'Create project'
let s:STRING__inlined__create_project = 'Create '.ediyalam#Name().' project'
let s:STRING__in_parent_menu__open_project = 'Open project'
let s:STRING__inlined__open_project = 'Open '.ediyalam#Name().' project'


function! ediyalam#tiers#nerdTree#main#init()

	let g:NERDTreeMinimalUI = 1
	" let g:NERDTreeQuitOnOpen = 1
	" let g:NERDTreeDirArrowExpandable = ''
	" let g:NERDTreeDirArrowCollapsible = ''

	call ediyalam#workspace#registerInit(function('s:on_VimEnter'))

	" let g:NERDTreeMenuUp = '<Up>'
	" let g:NERDTreeMenuDown = '<Down>'
endfunction

function s:on_VimEnter()
	call s:createNERDTreeMenus()
endfunction

let s:parent_menu = v:null

function s:addMenu(config)
	if ! s:inlined_menus
		let a:config.parent = s:parent_menu
	endif
	call NERDTreeAddMenuItem(a:config)
endfunction

function s:createNERDTreeMenus()

	if ! s:inlined_menus
		let s:parent_menu  = NERDTreeAddSubmenu({
				\ 'text': ediyalam#Name(),
				\ 'shortcut': 'z' })
	endif

	call s:addMenu({
	  \ 'text':     s:STRING__inlined__create_project,
	  \ 'shortcut': 'z',
	  \ 'callback': function('s:createProjectInDirectory'),
	  \ 'isActiveCallback': function('s:canCreateProjectInDirectory')
	  \ })

	call s:addMenu({
	  \ 'text':     s:STRING__inlined__open_project,
	  \ 'shortcut': 'z',
	  \ 'callback': function('s:openProjectFromDirectory'),
	  \ 'isActiveCallback': function('s:canOpenProjectFromDirectory')
	  \ })

	call NERDTreeAddMenuItem({
	  \ 'text':     '(R)un',
	  \ 'shortcut': 'R',
	  \ 'callback': function('s:executeFile'),
	  \ 'isActiveCallback': function('s:isFileExecutable')
	  \ })

	call NERDTreeAddMenuItem({
	  \ 'text':    '(D)ebug',
	  \ 'shortcut': 'D',
	  \ 'callback': function('s:startDebugFile'),
	  \ 'isActiveCallback': function('s:isFileDebuggable')
	  \ })

endfunction

" Highlight currently open buffer in NERDTree
" autocmd BufEnter * call SyncTree()
"
" statusline %{exists('b:NERDTree')?b:NERDTree.root.path.str():''}
"
" hide the first  line
"	augroup nerdtreehidecwd
"	autocmd!
"	autocmd FileType nerdtree setlocal conceallevel=3
"          \ | syntax match NERDTreeHideCWD #^[</].*$# conceal
"          \ | setlocal concealcursor=n
"	augroup end

function s:canOpenProjectFromDirectory()
	if ! ediyalam#project#isAProjectOpen() | return 0 | endif
	let node = g:NERDTreeFileNode.GetSelected()
	let path = node.path
	let p = path.str({'escape': 1})
	if ! ediyalam#project#directoryContainsAProject(p) | return 0 | endif
	return 1
endfunction
function s:openProjectFromDirectory()
endfunction

function s:canCreateProjectInDirectory()
	if ediyalam#project#isAProjectOpen() | return 0 | endif
	let node = g:NERDTreeFileNode.GetSelected()
	if ! node.path.isDirectory | return 0 | endif
	let path = node.path
	let p = path.str({'escape': 1})
	if ediyalam#project#directoryContainsAProject(p) | return 0 | endif
	return 1
endfunction
function s:createProjectInDirectory()
endfunction


function s:isFileExecutable()
	if ! ediyalam#project#isAProjectOpen() | return 0 | endif
	let node = g:NERDTreeFileNode.GetSelected()
	return node.path.isExecutable
endfunction

function s:isFileDebuggable()
	if ! ediyalam#project#isAProjectOpen() | return 0 | endif
	return s:isFileExecutable()
endfunction

function! s:startDebugFile()
    let node = g:NERDTreeFileNode.GetSelected()
    let path = node.path
    let p = path.str({'escape': 0})

    call ediyalam#debug#main#startGDB(p)
endfunction

function! s:executeFile()
    let node = g:NERDTreeFileNode.GetSelected()
    let path = node.path
endfunction

function! s:isNERDFocused()
	return bufname() =~ 'NERD_tree_\d\+'
endfunction
function! s:isNERDTreeOpen()
  return exists("t:NERDTreeBufName") && (bufwinnr(t:NERDTreeBufName) != -1)
endfunction

function! ediyalam#tiers#nerdTree#main#syncTree()
  if &modifiable && s:isNERDTreeOpen() && strlen(expand('%')) > 0 && !&diff
    NERDTreeFind
    wincmd p
  endif
endfunction

function ediyalam#tiers#nerdTree#main#activateDoNotReplaceNerdTree()
	" If another buffer tries to replace NERDTree, put it in the other window, and bring back NERDTree.
	autocmd BufEnter * if bufname('#') =~ 'NERD_tree_\d\+' && bufname('%') !~ 'NERD_tree_\d\+' && winnr('$') > 1 |
		\ let buf=bufnr() | buffer# | execute "normal! \<C-W>w" | execute 'buffer'.buf | endif
endfunction

function ediyalam#tiers#nerdTree#main#goInTabsOrClose()
	if s:isNERDTreeOpen()
		if s:isNERDFocused()
			NERDTreeClose
		else
			NERDTreeFocus
		endif
	else
		NERDTree
	endif
endfunction


