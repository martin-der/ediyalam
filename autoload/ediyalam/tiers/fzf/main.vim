
function ediyalam#tiers#fzf#main#ripgrep(query, fullscreen)
  let command_fmt = 'rg --column --line-number --no-heading --color=always --smart-case -- %s || true'
  let initial_command = printf(command_fmt, shellescape(a:query))
  let reload_command = printf(command_fmt, '{q}')
  let spec = {'options': ['--phony', '--query', a:query, '--bind', 'change:reload:'.reload_command]}
  call fzf#vim#grep(initial_command, 1, fzf#vim#with_preview(spec), a:fullscreen)
endfunction


function ediyalam#tiers#fzf#main#Init()
	"call fzf#run(fzf#wrap({'source': 'git ls-files --exclude-standard --others --cached'}))
	nmap <C-N> :FZF<CR>

	command! -nargs=* -bang RG call ediyalam#tiers#fzf#main#ripgrep(<q-args>, <bang>0)
endfunction
