

function ediyalam#tiers#cmake#main#init()
	let plugin = ediyalam#registerPlugin('cmake')
	let attribute = plugin.configuration.global.addAttribute('cmake-command', 'CMake Command', 'command')
	let attribute = plugin.configuration.global.addAttribute('default-target-directory', 'Default Target Directory', 'path')
	let attribute = plugin.configuration.project.addAttribute('configuration', 'Configuration', 'string')
	let attribute = plugin.configuration.project.addAttribute('target-directory', 'Target Directory', 'path')
	let attribute = plugin.configuration.project.addAttribute('source-directory', 'Source Directory', 'relative-path')
	      " .defaultTo('.')
endfunction

