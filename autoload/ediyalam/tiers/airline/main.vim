

if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif

function! s:airlineStatusLineFunc(...)
  let w:airline_section_x = get(w:, 'airline_section_x',    
        \ get(g:, 'airline_section_x', ''))    
  let w:airline_section_x .= ' %{WebDevIconsGetFileTypeSymbol()} '    
  " let w:airline_section_x .= ' Youk3 '    
  " let hasFileFormatEncodingPart = airline#parts#ffenc() !=? ''    
  " if g:webdevicons_enable_airline_statusline_fileformat_symbols && hasFileFormatEncodingPart    
	let w:airline_section_y = s:section_y 
  " endif
endfunction

if exists('g:loaded_airline') && g:loaded_airline == 1
  call airline#add_statusline_func(function('s:airlineStatusLineFunc'))
endif

let s:section_a = "%#__accent_bold#%{airline#util#wrap(ediyalam#tiers#airline#main#mode(),0)}%#__restore__#%{airline#util#append(airline#parts#crypt(),0)}%{airline#util#append(airline#parts#paste(),0)}%{airline#util#append(airline#extensions#keymap#status(),0)}%{airline#util#append(airline#parts#spell(),0)}%{airline#util#append('',0)}%{airline#util#append('',0)}%{airline#util#append(airline#parts#iminsert(),0)}"
let s:section_c = "%#__accent_bold#%{airline#util#wrap(ediyalam#tiers#airline#main#relativeBufname(),0)}%#__restore__#%{airline#util#append(airline#parts#crypt(),0)}%{airline#util#append(airline#parts#paste(),0)}%{airline#util#append(airline#extensions#keymap#status(),0)}%{airline#util#append(airline#parts#spell(),0)}%{airline#util#append('',0)}%{airline#util#append('',0)}%{airline#util#append(airline#parts#iminsert(),0)}"
let s:section_y = "%{airline#util#wrap(ediyalam#tiers#airline#main#ffenc(),0)}"
let s:section_z = "%{g:airline_symbols.linenr}%#__accent_bold#%l%#__restore__#/%L%{g:airline_symbols.maxlinenr}%#__accent_bold#%v%#__restore__#"

function! ediyalam#tiers#airline#main#Init()
	if ediyalam#util#system#canFancyFont()
		let g:airline_section_a = s:section_a
	endif
	let g:airline_section_a = s:section_a
	let g:airline_section_z = s:section_z
	let g:airline_section_y = s:section_y
	" let g:zairline_section_x = 

endfunction


function! ediyalam#tiers#airline#main#getSectionA()
	return s:section_a
endfunction

function! ediyalam#tiers#airline#main#getSectionC()
	return s:section_c
endfunction

function! ediyalam#tiers#airline#main#getSectionY()
	return s:section_y
endfunction

function! ediyalam#tiers#airline#main#getSectionZ()
	return s:section_z
endfunction

let s:relativeBufname_broken = 0
function! ediyalam#tiers#airline#main#relativeBufname()
	let filename = airline#extensions#fugitiveline#bufname()
	if s:relativeBufname_broken
		return l:filename
	endif
	try
		if ediyalam#project#isAProjectOpen() == 0
			return l:filename
		endif
		let rt = g:ediyalam_project.__rt
		let path = l:rt.path
		" Check if path is in project
		" then
		return "⌬ " . l:filename
		" else
		" return l:filename
	catch
		let s:relativeBufname_broken = 1
		echohl ErrorMsg | echom "relativeBufname broken : " . v:exception | echohl None
		return l:filename
	endtry
endfunction

function! ediyalam#tiers#airline#main#ffenc()
	let fileInfo = &fenc
	if strlen(&ff)
		let fileInfo =  fileInfo . ediyalam#tiers#airline#main#crStyle()
	endif
	return fileInfo
endfunction
function! ediyalam#tiers#airline#main#crStyle()
	let style = &ff

	if l:style ==# 'unix' | return g:ediyalam_carriage_return_text_unix | endif
	if l:style ==# 'mac'  | return g:ediyalam_carriage_return_text_mac | endif
	if l:style ==# 'dos'  | return g:ediyalam_carriage_return_text_dos | endif

	return '  '
endfunction

function! ediyalam#tiers#airline#main#mode()
	let mode = airline#parts#mode()

	if l:mode ==# 'INSERT'   | return '🖉   ' | endif
	if l:mode ==# 'REPLACE'  | return '🖉  ⌦' | endif
	if l:mode ==# 'VISUAL'   | return '⛶   ' | endif
	if l:mode ==# 'V-LINE'   | return '⛶ ☰ ' | endif
	if l:mode ==# 'V-BLOCK'  | return '⛶ ◻ ' | endif
	if l:mode ==# 'TERMINAL' | return '[> ]' | endif

	return '🗎   '
endfunction

