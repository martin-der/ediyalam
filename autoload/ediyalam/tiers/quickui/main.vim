

function ediyalam#tiers#quickui#main#Init()

	call quickui#menu#install('&File', [
            \ [ "&New File\tCtrl+n", 'echo 0' ],
            \ [ "&Open File\t(F3)", 'echo 1' ],
            \ [ "&Close", 'echo 2' ],
            \ [ "--", '' ],
            \ [ "&Save\tCtrl+s", 'echo 3'],
            \ [ "Save &As", 'echo 4' ],
            \ [ "Save All", 'echo 5' ],
            \ [ "--", '' ],
            \ [ "E&xit\tAlt+x", 'echo 6' ],
            \ ])
	" script inside %{...} will be evaluated and expanded in the string
	" ⌨ or 🖮
	call quickui#menu#install("&Option 🍀", [
			\ ['🖮 Edit &Shortcuts ', 'echo "Not yet"'],
			\ ['Set Spell %{&spell? "Off":"On"}', 'set spell!'],
			\ ['Set &Cursor Line %{&cursorline? "Off":"On"}', 'set cursorline!'],
			\ ['Set &Paste %{&paste? "Off":"On"}', 'set paste!'],
			\ ])

	" register HELP menu with weight 10000
	call quickui#menu#install('H&elp', [
			\ ["&Cheatsheet", 'help index', ''],
			\ ["&Ediyalam shortcuts", 'EdiyalamHelpShowKeymap', ''],
			\ ['T&ips', 'help tips', ''],
			\ ['--',''],
			\ ["&Tutorial", 'help tutor', ''],
			\ ['&Quick Reference', 'help quickref', ''],
			\ ['&Summary', 'help summary', ''],
			\ ], 10000)
	noremap <space><space> :call quickui#menu#open()<cr>

	" noremap <space><space> :call quickui#textbox#open(g:ediyalam_icon_text, {})<cr>
endfunction

