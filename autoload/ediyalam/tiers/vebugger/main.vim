

function s:prepareProject(event, data, helper)
	let projectName = a:event.project.name
	call ediyalam#util#log#notice("Load vebugger project '".l:projectName."'")
	call ediyalam#util#log#info('event : ' . json_encode(a:event))
	call ediyalam#util#log#info('data : ' . json_encode(a:data))
	call ediyalam#util#log#info(json_encode(a:helper.resources.project.list('.')))
	call a:helper.project.configuration.put('hello', { 'aaa': 12 })
endfunction

function s:saveProject(event, data, helper)
	call a:helper.resources.project.put('byebye.txt', "qda bs khgz egri\ner ub retoua al al igne") 
endfunction


function ediyalam#tiers#vebugger#main#Init()
	" let g:vebugger_breakpoint_text = '⬤ '
    " let g:vebugger_currentline_text = '=>'
	let g:vebugger_breakpoint_text = g:ediyalam_debugger_breakpoint_text
    let g:vebugger_currentline_text = g:ediyalam_debugger_currentline_text
	
	let plugin = ediyalam#registerPlugin('vebugger')
	call l:plugin.setProjectDefaultData({ 'breakpoints': [] , 'lastRuns': [] })
	call l:plugin.event.project.afterOpen(function('s:prepareProject'))
	call l:plugin.event.project.beforeClose(function('s:saveProject'))
endfunction

