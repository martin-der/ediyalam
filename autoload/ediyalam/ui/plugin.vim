
function ediyalam#ui#plugin#getString(label, default, extra)
	return input(a:label.': ')	
endfunction

let s:get_path_config = { 
	\ 'ui': {
		\ 'header': {
			\ 'isParagraph': 1
		\ }
	\ }
\}

function! s:complete_path(content, helper, data)
endfunction
function! s:validate_path(content, helper, data)
	if len(a:content)<=4
		call a:helper.addError('Beaucoup trop court')
		return
	endif
	if len(a:content)<=9
		call a:helper.addWarn('Encore un peu trop court')
		return
	endif
endfunction

function! s:complete_relative_path(content, helper, data)
endfunction
function! s:validate_relative_path(content, helper, data)
	if len(a:content)<=2
		call a:helper.addError('Beaucoup trop court')
		return
	endif
	if len(a:content)<=4
		call a:helper.addWarn('Encore un peu trop court')
		return
	endif
endfunction

function ediyalam#ui#plugin#getPath(label, default, extra)
	let config = deepcopy(s:get_path_config)
	let config.ui.header.text = a:label.' : '
	let config.content = { 'value' : a:default }
	let config.validation = { 'agent': function('s:validate_path') }
	let config.completion = { 'agent': function('s:complete_path') }
	" call extend(config, a:extra)
	return mdui#cmd#newStringController(config)
endfunction
function ediyalam#ui#plugin#getRelativePath(label, default, extra)
	let config = deepcopy(s:get_path_config)
	let config.ui.header.text = a:label.' : '
	let config.content = { 'value' : a:default }
	let config.validation = { 'agent': function('s:validate_relative_path') }
	let config.completion = { 'agent': function('s:complete_relative_path') }
	" call extend(config, a:extra)
	return mdui#cmd#newStringController(config)
endfunction

function ediyalam#ui#plugin#getSelection(choices, default, extra)
	let config = { 'content': { 'items': a:choices } }
	" call extend(config, a:extra)
	return mdui#cmd#newSelectOneController(config)
endfunction
function ediyalam#ui#plugin#getSelectionWithValidState(choices, default, extra) abort
	let config = {
	                \ 'content': { 'selection':a:default, 'items': a:choices },
	                \ 'ui': { 'drawItem': function('s:drawItemWithValidState') }
	           \ }
	let maxLength = 0
	for item in config.content.items
		let l = len(item.text)
		if l>maxLength
			let maxLength = l
		endif
	endfor
	let config.content.itemsTextMaxLength = maxLength
	" call extend(config, a:extra)
	return mdui#cmd#newSelectOneController(config)
endfunction

function s:drawItemWithValidState(item, state, config, data)
	echon a:item.text
	let lenDiff = a:config.content.itemsTextMaxLength - len(a:item.text)
	if lenDiff > 0
		echon repeat(' ',lenDiff)
	endif
	" ✓ (U+2713)
	" ✗ (U+2717)
	" ✔ (U+2714)
	" ✘ (U+2718)
	" ⚠ 
	echon ' '
	if a:item.isValid
		echohl EdiyalamMessageDone | echon '✔' | echohl None
	else
		echohl EdiyalamMessageError | echon '✘' | echohl None
		" echohl EdiyalamMessageError | echon '⚠' | echohl None
	endif
endfunction

function ediyalam#ui#plugin#getBoolean(label, extra)
	return 0
endfunction
