
" If another plugin calls an autoloaded Ediyalam function on startup before the
" plugin/ediyalam.vim file got loaded, load it explicitly
if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif



function! ediyalam#util#getNextLoopingInt(currentInt,intCount)
	if a:intCount < 1
		return -1
	endif
	let nextInt = a:currentInt + 1
	if nextInt < 0 || nextInt >= a:intCount
		let nextInt = 0
	endif
	return nextInt
endfunction


" String manipulation

function! ediyalam#util#stringPosition(searched, strings)
	let index = 0
	for string in a:strings
		if string == a:searched
			return index
		endif
		let index = index + 1
	endfor
	return -1
endfunction
function! ediyalam#util#getNextString(string, strings)
	let stringCount = len(a:strings)
	let index = ediyalam#util#stringPosition(a:string, a:strings)
	let index = ediyalam#util#getNextLoopingInt(index,stringCount)
	if index < 0 
		return ''
	endif
	return a:strings[index]
endfunction



""""""""
" @return 's' prededed by n times ' '
function! ediyalam#util#Pad(s,n)
    return a:s . repeat(' ',a:n - len(a:s))
endfunction

""""""""
" @return if '...' is supplied, 's' followed by n times the value of first '...'
"         if not,               's' followed by n times ' '
function! ediyalam#util#PrePad(s,n,...)
    if a:0 > 0
        let char = a:1
    else
        let char = ' '
    endif
    return repeat(char,a:n - len(a:s)) . a:s
endfunction

