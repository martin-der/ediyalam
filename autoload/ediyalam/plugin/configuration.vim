
let s:ConfigurationDescription = {}
let s:Attribute = {}
let s:ConfigurationDescription.attributes = []



function ediyalam#plugin#configuration#createConfigurationDescription()
	let configuration = deepcopy(s:ConfigurationDescription)
	return configuration
endfunction

function s:Attribute.defaultTo(value) dict
	self.defaultValue = a:value
	return self
endfunction
function s:Attribute.validateWith(fctValidator) dict
	self.validator = a:fctValidator
	return self
endfunction

function s:validateAttributeName(name)
	if a:name ==# ''
		throw "Invalid name '" . a:name . "' : Blank names are not allowed"
	endif
	if !(a:name =~ '^[a-zA-Z]\+\([a-zA-Z01-9\-]*[a-zA-Z01-9]\)\?$')
		throw "Invalid name : Must contain only 'a..z', 'A..Z', '-', '0..9' with no '-' at the beginning or the end and no number at the beginning"
	endif
endfunction

function s:ConfigurationDescription.addAttribute(name, label, type) dict abort
	call s:validateAttributeName(a:name)
	for a in self.attributes
		if a.name ==# a:name
			throw "A attribute named '".a:name."' already exists"
		endif
	endfor
	let attribute = copy(s:Attribute)
	let attribute.name = a:name
	let attribute.type = a:type
	let attribute.label = a:label
	call add(self.attributes, attribute)
	return attribute
endfunction

