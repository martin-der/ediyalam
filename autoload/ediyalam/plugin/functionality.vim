
let s:Functionality = {
\	'methods' : {},
\	'attributes' : {}
\}

let ediyalam#plugin#functionality#Functionality = s:Functionality
function ediyalam#plugin#functionality#Functionality()
	return s:Functionality
endfunction


function! s:Functionality.New(type) dict
    let functionality = deepcopy(self)
    call functionality._init(a:type)
    return functionality
endfunction

function s:Functionality._init(type) dict
	let self.type = a:type
endfunction

function s:Functionality.method(name, mandatory, args) dict abort
	let t_args = type(a:args)
	if t_args == v:t_string
		if !(a:args =~ '^[01-9]\{1,3}+\?$')
			throw "Invalid 'args' value (expected an integer between 0 and 999, followed by an optional '+') : '".a:args."'"
		endif
		let args_count = str2nr(a:args[:-2])
		let variadic = a:args =~ '^.*+$'
	elseif t_args == v:t_number
		let args_count = t_args
		let variadic = 0
	else
		throw 'Not a valid args parameter with type='.t_args
	endif
	let self.methods[a:name] = { 'mandatory': a:mandatory, 'argsCount': args_count, 'variadic': variadic }
	" let t_args = type(a:args)
	" if t_args != v:t_list
	" 	throw 'Not a valid args parameter : expected a List not a '.t_args
	" endif
	" let finit_args = a:args
	" let variadic = 0
	" if len(finit_args)>0 && (finit_args[-1] is v:null || finit_args[-1] < 0)
	" 	let finit_args = finit_args[:-2]
	" 	let variadic = 1
	" endif
	" let self.methods[a:name] = { 'mandatory': a:mandatory, 'args': finit_args, 'variadic': variadic }
endfunction



let s:FunctionalityProvider = { '_': {} }
function ediyalam#plugin#functionality#FunctionalityProvider()
	return s:FunctionalityProvider
endfunction

function! s:FunctionalityProvider.New(functionality, context, builderCallback) dict
    let functionalityProvider = deepcopy(self)
    call s:FunctionalityProvider_init(functionalityProvider, a:functionality, a:context, a:builderCallback)
    return functionalityProvider
endfunction

function s:FunctionalityProvider_init(functionalityProvider, functionality, context, builderCallback)
	let a:functionalityProvider._.provider = s:buildAndCheckProvider(a:functionality, a:context, a:builderCallback)
	let a:functionalityProvider._.functionality = a:functionality
endfunction

function s:buildAndCheckProvider(functionality, context, builderCallback) abort
	let provider = {}
	call a:builderCallback(a:context, provider)
	" TODO check methods, attributes...
	return provider
endfunction

function ediyalam#plugin#functionality#delegate(provider, name, ...)
	let ProviderFunction = a:provider._.provider[a:name]
	let args = []
	let context = {'functionality': a:provider._.functionality}
	let argsWithContext = [context] + a:000
	return call(ProviderFunction, argsWithContext)
endfunction

