

let s:Manager = {}

function! s:Manager.New(name, configurationDescription) dict
    let manager = copy(s:Manager)
	call extend(manager, ediyalam#plugin#plugin#createTypedBroker(a:name, 'project-manager'))
	call s:_init(manager)
    return manager
endfunction

function s:_init(manager) dict
endfunction




let s:Module = {}

function! s:Module.New(name) dict
    let module = copy(self)
    call module._init(a:name)
    return module
endfunction

function s:Module._init(name) dict
	let self.name = a:name
endfunction

