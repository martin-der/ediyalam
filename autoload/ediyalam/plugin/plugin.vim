
if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif



let s:Broker = {}
let s:Broker.event = {}
let s:Broker.event.project = {}
let s:Broker.configuration = {}
let s:Broker.configuration.global = ediyalam#plugin#configuration#createConfigurationDescription()
let s:Broker.configuration.project = ediyalam#plugin#configuration#createConfigurationDescription()


let s:brokers = []


function ediyalam#plugin#plugin#createBroker(name)
	if a:name is v:null
		throw "Plugin name is mandatory"
	endif
	call s:validateName(a:name)
	
	for broker in s:brokers
		if broker.name ==# a:name
			throw "A plugin named '" . a:name ."' already exists"
		endif
	endfor
	let newBroker = s:_new(a:name)
	let s:brokers = add(s:brokers, newBroker)
	let newBroker.data[a:name] = {}
	" call ediyalam#util#log#debug("Register plugin '".a:name."'")
	return newBroker
endfunction
function ediyalam#plugin#plugin#createTypedBroker(name, type) abort
	let broker = ediyalam#plugin#plugin#createBroker(name)
	let broker.type = a:type
	return broker
endfunction

function ediyalam#plugin#plugin#getBrokerOrThrow(name) abort
	let broker = ediyalam#plugin#plugin#getBroker(a:name)
	if broker is v:null | throw "No such plugin '".a:name."'" | endif
	return broker
endfunction
function ediyalam#plugin#plugin#getBroker(name) abort
	for broker in s:brokers
		if a:name ==# broker.name
				return broker
		endif
	endfor
	return v:null
endfunction

function ediyalam#plugin#plugin#removeAll()
	let s:brokers = []
endfunction

function s:validateName(name)
	if a:name =~ '\(^\|/\)\.\+\(/\|$\)'
		throw "Invalid name '" . a:name . "' : Having only '.' at the beginning, at the end or between '/' is not allowed."
	endif
	if a:name =~ '^ *$'
		throw "Invalid name '" . a:name . "' : Blank names are not allowed"
	endif
	if a:name =~ '\(^\|/\) \+\(/\|$\)'
		throw "Invalid name '" . a:name . "' : Having only spaces at the beginning, at the end or between '/' is not allowed."
	endif
	if a:name =~ '^/\|/$'
		throw "Invalid name '" . a:name . "' : Name cannot start or end with '/'."
	endif
	if a:name =~ '[^a-zA-Z01-9\._\-/\$#]'
		throw "Invalid name '" . a:name . "' : Must contains only a-zA-Z0-9._-/$#"
	endif
endfunction

function s:_new(name)
	call s:validateName(a:name)
	let broker = deepcopy(s:Broker)
	call s:init(l:broker, a:name)
	call s:setParentReferenceInChildren(l:broker)
	return l:broker
endfunction

function s:init(broker, name)
	let a:broker.name = a:name
	let a:broker.data = {}
	let a:broker.defaultData = {}
	let a:broker.defaultData.project = v:null
	let a:broker.log = ediyalam#util#log#newPluginLog('['.a:name.'] ', 'info')
endfunction

function s:setParentReferenceInChildren(broker)
	let a:broker.event.project.__p = a:broker
endfunction

function s:Broker.setProjectDefaultData(data)
	if type(a:data) != v:t_dict
		throw "'data' must be a dict"
	endif
	let self.defaultData.project = a:data
endfunction

function ediyalam#plugin#plugin#initPluginsProjectInternal(project)
	for broker in s:brokers
		if ! (broker.defaultData.project is v:null)
			let a:project.__rt.pluginData[broker.name] = deepcopy(broker.defaultData.project)
		endif
		let pluginResourcesProvider = ediyalam#util#resource#newResourcesProvider(a:project.pluginDataPath(broker.name))
		let a:project.__rt.resourcesProvider.plugin[broker.name] = pluginResourcesProvider
		let pluginConfigurationProvider = ediyalam#util#configuration#newConfigurationProvider(a:project.pluginConfigurationPath(broker.name))
		let a:project.__rt.configurationProvider.plugin[broker.name] = pluginConfigurationProvider
		let pluginConfigurationConfigurationProvider = ediyalam#util#configuration#newConfigurationProvider(a:project.pluginConfigurationConfigurationPath(broker.name))
		let a:project.__rt.configurationProvider.pluginConfiguration[broker.name] = pluginConfigurationConfigurationProvider

		call pluginResourcesProvider.mkDirs('.')
	endfor
endfunction


function ediyalam#plugin#plugin#logException(plugin, when, problem)
	call ediyalam#util#log#error("[P:" . a:plugin.name . "] " . a:when . " : " . a:problem.where . " : " . a:problem.what)
endfunction

function s:Broker.event.project.afterOpen(cb) dict
	call ediyalam#event#registerProjectAfterOpenListener(self.__p, a:cb)
endfunction
function s:Broker.event.project.beforeClose(cb) dict
	call ediyalam#event#registerProjectBeforeCloseListener(self.__p, a:cb)
endfunction


function ediyalam#plugin#plugin#applyToProject(pluginName, project) abort
	let plugin = ediyalam#plugin#plugin#getBrokerOrThrow(a:pluginName)
	call ediyalam#plugin#plugin#configureForProject(plugin, a:project)
endfunction

function ediyalam#plugin#plugin#configure(plugin)
endfunction
function ediyalam#plugin#plugin#configureForProject(plugin, project) abort
	if a:project is v:null | throw "Nul Project" | endif
	let configurationProvider = a:project.__rt.configurationProvider.plugin[a:plugin.name]
	let config = configurationProvider.getOr('config', {})
	let pluginConfigurationAttributes = a:plugin.configuration.project.attributes


	let selectedIndex = 0
	while 1
		let selectItems = []
		for attribute in pluginConfigurationAttributes
			call add(selectItems, {'text':attribute.label, 'attribute': attribute, 'isValid':0})
		endfor
		let choice = ediyalam#ui#plugin#getSelectionWithValidState(selectItems, selectedIndex, {}).run()
		if choice is v:null
			break
		endif
		let selectedIndex = choice.index
		let defaultValue = has_key(config, choice.item.attribute.name) ? config[choice.item.attribute.name] : 'azeaz'
		let result = ediyalam#ui#plugin#getPath(choice.item.text, defaultValue, {}).run()

		let config[choice.item.attribute.name] = {'value': result}
		call configurationProvider.put('config', config)
	endwhile
	return selectedIndex
endfunction

