



if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif


let s:callback = {}
let s:callback.project = {}
let s:callback.project.afterOpen = []
let s:callback.project.beforeClose = []

function s:checkCallBackParameter(cb)
	let t = type(a:cb)
	if t != v:t_func
		throw "Listener must be a function (not a ".t.")"
	endif
endfunction

function s:listener(plugin, cb)
	return { 'plugin' : a:plugin, 'callback' : a:cb }
endfunction

function s:notifyEventGuarded(listener, event, context, inScript)
	let pluginName = a:listener.plugin.name
	let data = { 'plugin' : a:listener.plugin.data[l:pluginName] }
	let helper = { 'resources' : {} }
	
	if has_key(a:context, 'project')
		call extend(data, { 'project' : a:context.project.__rt.pluginData[l:pluginName] } )
		let helper.resources.project = a:context.project.__rt.resourcesProvider.plugin[pluginName]
		let helper.project = {}
		let helper.project.configuration = a:context.project.__rt.configurationProvider.plugin[pluginName]
	endif

	let RefCb = a:listener.callback
	try
		call RefCb(a:event, l:data, l:helper)
	catch
		" get current function : 
		let thisFunction = substitute(a:inScript, '.*\(\.\.\|\s\)', '', '')
		let problem = { 'what': v:exception, 'where' : v:throwpoint }
		call ediyalam#plugin#plugin#logException(a:listener.plugin, l:thisFunction, l:problem)
	endtry
endfunction
function s:notifyAllEventGuarded(listeners, event, context, inScript)
	for listener in a:listeners
		call s:notifyEventGuarded(listener, a:event, a:context, a:inScript)
	endfor
endfunction

function ediyalam#event#registerProjectAfterOpenListener(broker, cb)
	call s:checkCallBackParameter(a:cb)
	let s:callback.project.afterOpen = add(s:callback.project.afterOpen, s:listener(a:broker, a:cb))
endfunction
function ediyalam#event#notifyProjectAfterOpen(project)
	let event = { 'type': 'project/open', 'project': a:project.simpleView() }
	let context = { 'project' : a:project }
	call s:notifyAllEventGuarded(s:callback.project.afterOpen, event, context, expand('<sfile>'))
endfunction

function ediyalam#event#registerProjectBeforeCloseListener(broker, cb)
	call s:checkCallBackParameter(a:cb)
	let s:callback.project.beforeClose = add(s:callback.project.beforeClose, s:listener(a:broker, a:cb))
endfunction
function ediyalam#event#notifyProjectBeforeClose(project)
	let event = { 'type': 'project/close', 'project': a:project.simpleView() }
	let context = { 'project' : a:project }
	call s:notifyAllEventGuarded(s:callback.project.beforeClose, event, context, expand('<sfile>'))
endfunction

