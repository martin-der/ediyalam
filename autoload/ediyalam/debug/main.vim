

if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif


function! s:getArguments()
    let arguments = input("Enter arguments:", "")
	return split(arguments, " ")
endfunction

function ediyalam#debug#main#startGDB(exec_file)
	if ediyalam#debug#debug#isDebugRunning()
		let choice=confirm("A debug session is running. Stop it and start another session ?","&Restart\n&No",2)
		if choice == 2
			return
		endif
		call ediyalam#debug#debug#KillGDB()
	endif
	let arguments = s:getArguments()
	call ediyalam#debug#debug#StartGDB(a:exec_file, arguments)
endfunction
