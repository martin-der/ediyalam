scriptencoding utf-8

if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif

let s:log = ediyalam#util#log#newPluginLog('vebugger ', 'debug')

let s:last_evaluated_expression = v:null
let s:watched_expressions=[]

let s:instance_vebugger = v:null

function s:notifyErrorNotDebugging()
	call ediyalam#util#log#error('No running debug session')
endfunction

function s:callCallbackAndDestroyIfAny(instance, result)
	if has_key(a:instance.__ediyalam, 'caller')
		let caller = a:instance.__ediyalam.caller
		call caller.cb(result, caller.data)
		remove(a:instance.__ediyalam, 'caller')
	endif
endfunction

function ediyalam#debug#debug#isDebugRunning()
	if s:instance_vebugger is v:null
		return 0
	else
		return 1
	endif
endfunction

function s:handle_read(pipe, line, readResult, instance)
	if a:pipe !=# 'out' | return | endif
	let l:data = a:instance.__ediyalam
	if l:data.parseMode is v:null
		if a:line ==# '&info break'
			let l:data.parseMode = 1
			let l:data.parseStep = 0
			let l:data.parseData = []
			return
		endif
		return
	endif

	if a:line ==# "^done"
		if a:instance.__ediyalam.parseMode == 1
			let a:readResult.breakpoints = l:data.parseData
		endif
		let a:instance.__ediyalam.parseMode = v:null
	endif

	if a:instance.__ediyalam.parseMode == 1
		if l:data.parseStep == 0
			if a:line =~ '^\~Num'
				let l:data.parseStep = 1
			else
				let a:instance.__ediyalam.parseMode = v:null
				call s:log.error("Failed to parse breakpoint header, expected '^\~Num' found '".a:line."'")
			endif
			return
		endif
		if l:data.parseStep == 1
			" '~2       breakpoint     keep y   0x0000555555555155 in main at src/bar.c:11'
			try
				let m = matchlist(a:line, '^\~\([01-9]\+\)[ \t]\+\(breakpoint\)[ \t]\+\(keep .\)[ \t]\+\(0x[01-9a-fA-F]\+\)[ \t]\+\(.\+\).*$')
				let pos = m[5]
				let m = matchlist(l:pos, '^in \([^ ]\+\) at \(.\+\):\([01-9]\+\)$')
				let fct_name = m[1]
				let file_name = m[2]
				let line_number = m[3]
				call add(l:data.parseData, { 'file': file_name, 'line': line_number, 'function': fct_name })
			catch
				call s:log.error("Failed to parse breakpoint line '".a:line."' : ".v:exception)
			endtry
		endif
	endif

	" let dmsg = 'Handle_read evaluation'
	" let dmsg = dmsg . ' pipe = '.a:pipe
	" let dmsg = dmsg . ' line = "'.a:line.'"'
	" let dmsg = dmsg . ' readResult = '.json_encode(a:readResult)
	" let dmsg = dmsg . ' instance.__ediyalam = '.json_encode(a:instance.__ediyalam)
	" call ediyalam#util#log#ok(dmsg . "\n")
endfunction

function s:handle_think(result, instance)
	if ediyalam#debug#debug#isDebugRunning() == 0 | call s:notifyErrorNotDebugging() | return | endif
	" echo a:result
	if has_key(a:result.std, 'breakpoints')
		call s:log.info('breakpoints : ' . json_encode(a:result.std.breakpoints))
		call s:log.info(json_encode(l:data.parseData))
		call s:callCallbackAndDestroyIfAny(a:instance, a:result.std.breakpoints)
	endif
	if a:result.std.evaluatedExpression != {}
		echo 'New evaluation : '.(a:result.std.evaluatedExpression.expression).'='.(a:result.std.evaluatedExpression.value)
	endif
endfunction

function s:currentInstance()
	return s:instance_vebugger
endfunction

function s:fetchInfo(what, cb, data)
	let instance = s:currentInstance()
	let instance.__ediyalam.caller.cb = a:cb
	let instance.__ediyalam.caller.data = a:data
	if a:what ==# 'breakpoints'
		call instance.writeLine('info break')
		return
	endif
endfunction

function ediyalam#debug#debug#fetchBreakpoints(cb, data)
	s:getInfo('breakpoints', a:cb, a:data)
endfunction


function ediyalam#debug#debug#getBreakpoints()
	return copy(g:vebugger_breakpoints)
endfunction
function ediyalam#debug#debug#setBreakpoints(breakpoints)
	let breakpoints_copy = copy(a:breakpoints)
	" FIXME why do we have to do this twice ?
	for b in breakpoints_copy
		call vebugger#std#updateMarksForFile({}, b.file)
	endfor
	let g:vebugger_breakpoints = breakpoints_copy
	for b in g:vebugger_breakpoints
		call vebugger#std#updateMarksForFile({}, b.file)
	endfor
endfunction

function ediyalam#debug#debug#StartGDB(exec_file, exec_args)
	let arguments = {'args':a:exec_args}
	call ediyalam#util#log#done("Debugging '" . a:exec_file . "'")
	let l:instance = vebugger#gdb#start(a:exec_file, arguments)
	let l:instance.__ediyalam = { 'parseMode': v:null }
	let l:instance.__ediyalam.caller = {}
	call l:instance.addReadHandler(function('s:handle_read'))
	call l:instance.addThinkHandler(function('s:handle_think'))
	let s:instance_vebugger = l:instance
	return l:instance
endfunction
function ediyalam#debug#debug#KillGDB()
	if ! ediyalam#debug#debug#isDebugRunning() | call s:notifyErrorNotDebugging() | return | endif
	VBGkill
	let s:instance_vebugger = v:null
endfunction


function ediyalam#debug#debug#StepOver()
	if ! ediyalam#debug#debug#isDebugRunning() | call s:notifyErrorNotDebugging() | return | endif
	VBGstepOver
endfunction
function! ediyalam#debug#debug#StepIn()
	if ! ediyalam#debug#debug#isDebugRunning() | call s:notifyErrorNotDebugging() | return | endif
	VBGstepIn
endfunction
function! ediyalam#debug#debug#StepOut()
	if ! ediyalam#debug#debug#isDebugRunning() | call s:notifyErrorNotDebugging() | return | endif
	VBGstepOut
endfunction
function! ediyalam#debug#debug#Continue()
	if ! ediyalam#debug#debug#isDebugRunning() | call s:notifyErrorNotDebugging() | return | endif
	VBGcontinue
endfunction

function! ediyalam#debug#debug#ToggleBreakpoint(file,line)
	VBGtoggleBreakpoint a:file a:line
endfunction
function! ediyalam#debug#debug#ToggleBreakpointThisLine()
	VBGtoggleBreakpointThisLine
endfunction
function! ediyalam#debug#debug#RemoveBreakpoints()
	VBGclearBreakpoints
endfunction


function! ediyalam#debug#debug#EvaluateExpression(text)
	vebugger#std#eval(a:text)
endfunction
function! ediyalam#debug#debug#Evaluate()
	let text = input('Evaluate : ')
	VBGeval text
	let s:last_evaluated_expression=selection
endfunction
function! ediyalam#debug#debug#evaluateSelected()
	let selection = ediyalam#util#vimy#getVisualSelection()
	echo 'selection = ['.selection.']'
	VBGeval a:selection
	let s:last_evaluated_expression=selection
endfunction
function! ediyalam#debug#debug#EvaluateLast()
	if (s:last_evaluated_expression)
		VBGeval s:last_evaluated_expression
	else
		call ediyalam#util#system#messageError('No previous evaluated expression')
	endif
endfunction


function! ediyalam#debug#debug#ListWatchedExpressions()
	echo 'Watched expressions'
	for expression in s:watched_expressions
		echo expression
	endfor
endfunction
function! ediyalam#debug#debug#AddWatchedExpression(expression)
	call extend(s:watched_expressions , [{'expression':a:expression, 'value':''}])
endfunction
function! ediyalam#debug#debug#ClearWatchedExpressions()
	let s:watched_expressions = []
endfunction
function! ediyalam#debug#debug#ShowWatchedExpressions()

	redir => content
		":silent call ediyalam#debug#debug#ListWatchedExpressions()
		silent echo "Expressions :"
		for expression in s:watched_expressions
			silent echo '- '.(expression.expression).': '.(expression.value)
		endfor
	redir END

	:new
	:set modifiable
	:set noswapfile
	silent put=content
	":silent file KeyMaps
	silent filetype=ediyalam_watched_expressions
	:set bufhidden=hide
	:set buftype=nofile
	:set nomodifiable
	:set nobuflisted
	":silent set syntax=vim
	:map <buffer><silent> q :bdelete<CR>

endfunction



