" ============================================================================
" File:        process.vim
" Description: 
" Author:      
" Licence:     Vim licence
" Website:     
" Version:     0.0.1
" Note:        
"              
" ============================================================================

scriptencoding utf-8


if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif





"autocmd BufReadPost quickfix nnoremap <buffer> <CR> <CR>


function! ediyalam#process#ProcGetMonitorBufferNumber()
	for buffer_number in range(1, bufnr('$'))

		let buftype = getbufvar(buffer_number,"&buftype")
		let filetype = getbufvar(buffer_number,"&filetype")

		if filetype == 'processes'
			return buffer_number
		endif

	endfor
endfunction

function! ediyalam#process#ProcList()
    let result = []

	"	filter(b_all, 'buflisted(v:val)')

    for buffer_number in range(1, bufnr('$'))
		"let type = buffiletype(buffer_number)
		let buftype = getbufvar(buffer_number,"&buftype")
		let filetype = getbufvar(buffer_number,"&filetype")
		"echo 'type buf= '.buftype.' file='.filetype
	
		"if buflisted(buffer_number)
		"    continue
		"endif
		if buftype != 'nofile'
			continue
		endif

		if filetype != 'process' && filetype != 'processes'
			continue
		endif

        call add(result, ':::'.buftype.':'.filetype.' - '.bufname(buffer_number))
   endfor

    return result
endfunction


function! ediyalam#process#ProcShow()
	let text = ProcList()

	echo text

endfunction


function! ediyalam#process#ProcMonitorUpdate() 
	redir =>output
	silent exec ProcShow()
	redir  END
	return output
endfunction

function! ediyalam#process#ProcMonitorGet()
	"new
	silent split __PROCESS__
	silent file "processes"
	let num = bufnr('%')
	nnoremap <buffer> <silent> q :q<CR>
	"echo 'num = '.num
	"call setbufvar(num,"&readonly",1)
	call setbufvar(num,"&swapfile",0)
	call setbufvar(num,"&buflisted",0)
	call setbufvar(num,"&filetype", "processes")
	call setbufvar(num,"&buftype", "nofile")
	"call setbufvar(num,"&bufhidden","hide")
	call setbufvar(num,"&modifiable",0)

endfunction

function! ediyalam#process#ProcAdd()

	silent call ProcMonitorGet()

endfunction




""" 

function ediyalam#process#QuickLaunch()

	"let runLabel = forms#newLabel({'text': ' |> '})
	let runLabel = forms#newLabel({'text': '   '})
	let run = forms#newButton({
								\ 'tag': 'processLaunch',
								\ 'body': runLabel}) 

	let debugLabel = forms#newLabel({'text': ' ╣) '})
	let debugLabel = forms#newLabel({'text': ' ╣) '})
	let debug = forms#newButton({
								\ 'tag': 'processLaunch',
								\ 'body': debugLabel})

	let profileLabel = forms#newLabel({'text': ' Oo '})
	let profile = forms#newButton({
									\ 'tag': 'processLaunch',
									\ 'body': profileLabel,
									\ 'action': g:forms#cancelAction})


	let launchers = forms#newHPoly({ 'children': [run, debug, profile] })

	let knownLaunchables = {
			\ 'choices' : [
			\   ["-- Add new Launch --", 11],
			\   ["ONE", 1],
			\   ["TWO", 2],
			\   ["THREE", 3],
			\   ["FOUR", 4],
			\   ["FIVE", 5],
			\   ["SIX", 6],
			\   ["SEVEN", 7],
			\   ["EIGHT", 8],
			\   ["NINE", 9],
			\   ["TEN", 10]
			\ ],
			\ 'size' : 8, 'pos' : 0,
			\ 'on_selection_action' : 'echo'
			\ }
	let launchablesList =	forms#newPopDownList(knownLaunchables)

	let content = forms#newHPoly({ 'children': [launchablesList, launchers] })

	let border = forms#newBox({ 'body': content, 'mode': 'light_arc' })

	let bg = forms#newBackground({ 'body': border})


	let form = forms#newForm({'body': bg })

"	call forms#AppendInput({'type': 'Sleep', 'time': 5})
"	call forms#AppendInput({'type': 'Exit'})
"

	call form.run()
endfunction


