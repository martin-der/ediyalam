" ============================================================================
" File:        help.vim
" Description: 
" Author:      
" Licence:     Vim licence
" Website:     
" Version:     0.0.1
" Note:        
"              
" ============================================================================

scriptencoding utf-8


" If another plugin calls an autoloaded Ediyalam function on startup before the
" plugin/ediyalam.vim file got loaded, load it explicitly
if exists(':Ediyalam') == 0
    runtime plugin/ediyalam.vim
endif

function! ediyalam#help#showKeymap()
	redir => allMapping                                                                                                                
	:silent map                                                                                                                        
	redir END                                                                                                                          
	let mappings = split(allMapping,"\n")                                                                                              
	let filteredMappings = filter(copy(mappings), 'stridx(v:val, ":Ediyalam")>-1')
	call fzf#run({'source': filteredMappings,                                                                                          
		\ 'options': ['--header','Command and shortcut', '--prompt', '⌨ > '],                                                      
		\ 'left': '25%',                                                                                                           
		\ 'window': {'width':0.8, 'height':0.8, 'border':'rounded'}})                                                              


	" if nr >= 0
	" 	call win_gotoid(nr)
	" 	return
	" endif

	" redir => message
	" :silent map
	" redir END

	" :new
	" :set modifiable
	" :set noswapfile
	" silent put=message
	" :silent v/:Ediyalam/d
	" :silent file Ediyalam_KeyMaps
	" :set bufhidden=hide
	" :set buftype=nofile
	" :set nomodifiable
	" :set nobuflisted
	" :silent set syntax=vim
	" :map <buffer><silent> q :bdelete<CR>

endfunction



