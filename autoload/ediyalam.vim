" ============================================================================
" File:        ediyalam.vim
" Description: 
" Author:      
" Licence:     Vim licence
" Website:     
" Version:     0.0.1
" Note:        
"              
" ============================================================================

scriptencoding utf-8

if exists('g:autoloaded_ediyalam') || &compatible
	finish
endif
let g:autoloaded_ediyalam = 1
let s:initialized = 0

runtime plugin/ediyalam.vim

let s:version = 001

function! ediyalam#Version()
	return s:version
endfunction

function! ediyalam#Name()
	return 'Ediyalam' 
endfunction

let s:projects_info_default = {'known': []}
let s:projects = copy(s:projects_info_default)
let s:current_project_index=-1

let g:ediyalam_icon_text = [
\'┏━╸━━╸⬮╺━━━━━━━━━━━┏┳┓',
\'┣╸ ╺┳┓╻┗┳┛┏━┓╻  ┏━┓┃┃┃',
\'┗━╸╺┻┛┃ ┛ ┣━┫┗━╸┣━┫╹ ╹' ]
let g:ediyalam_icon_text_2 = [
\'╭─╴──╴•╶───────────┌┬╮',
\'├╴ ╶┬╮╷╰┬╯╭─╮╷  ╭─╮│││',
\'╰─╴╶┴╯│ ┘ ├─┤╰─╴├─┤╵ ╵' ]
let g:ediyalam_icon_text_3 = [
\'╔═╕══╛⬯╘═══════════╔╦╗',
\'╠╕ ╘╦╗╓╚╦╝╔═╗╓  ╔═╗║║║',
\'╚═╛╘╩╝║ ╝ ╠═╣╚═╛╠═╣╙ ╜' ]


function ediyalam#showSlashScreen()
	let splash = readfile(ediyalam#resourcePath('logo.txt'))
	let name = copy(g:ediyalam_icon_text_2)
	let git_version = system('git describe --tags --always')
	let name[1] = name[1] . ' ' . l:git_version
	let splash += name
	call quickui#textbox#open(splash, {})
endfunction

let s:resource_directory = fnamemodify(resolve(expand('<sfile>:p')), ':h') . '/../resource'

function ediyalam#resourcePath(resource)
	return s:resource_directory . '/' . a:resource
endfunction

let s:STR_no_project_selected = 'No project selected'

let g:ediyalam_project = v:null

function s:setDefaultConfiguration(feature, value)
	let m = matchlist(a:feature,'^\([^~]\+\)\~\(.*\)$')
	let modif = v:null
	if len(m)>0
		let name = "g:ediyalam_".m[1]
		let modif = m[2]
	else 
		let name = "g:ediyalam_".a:feature
	endif
	
	if exists(name)
		return
	endif

	if modif is v:null
		let l:value_type = type(a:value)
		if l:value_type == v:t_number
			exec "let " . name . " = " . a:value
		elseif l:value_type == v:t_float
		exec "let " . name . " = " . a:value
		elseif l:value_type == v:t_string
			exec "let " . name . " = '" . a:value . "'"
		endif
	else
		try
			if modif ==# 'F>T'
				if g:ediyalam_term_use_fancy_font
					if g:ediyalam_term_use_nerd_font
						exec "let " . name . " = '" . a:value[2] . "'"
					else
						exec "let " . name . " = '" . a:value[1] . "'"
					endif
				else
					exec "let " . name . " = '" . a:value[0] . "'"
				endif
			else
				echohl ErrorMsg | echom "Unknown default parameter modifier '".modif."'" | echohl None
			endif
		catch
			echohl ErrorMsg | echom "Bad default parameter modifier '".modif."' for '".name."' : ".v:exception | echohl None
		endtry
	endif
endfunction

let s:defaultConfigurations = [
	\ ['highlight_message_debug', '#4e4e4e - -'],
	\ ['highlight_message', '#070707 - -'],
	\ ['highlight_message_ok', '#e8e8e8 - -' ],
	\ ['highlight_message_notice', '#70a7e0 - -' ],
	\ ['highlight_message_done', '#28a745 - -' ],
	\ ['highlight_message_info', '#17a2b8 - -' ],
	\ ['highlight_message_warn', '#ffc107 - -' ],
	\ ['highlight_message_error', '#dc3545 - -' ],
	\
	\ ['on_enter_load_cwd_project', 0 ],
	\ ['on_enter_load_previous_project', 0 ],
	\
	\ ['visual_on_start', 1],
	\ ['visual_text_marker_level', 0],
	\ ['visual_gutter', 1],
	\ ['visual_tab_width', 2],
	\ ['visual_theme_index', 0],
	\ ['visual_theme_airline_index', 0],
	\
	\ ['term_use_fancy_font', ediyalam#util#system#canFancyFont() ],
	\ ['term_use_nerd_font', 0 ],
	\
	\ ['debugger_breakpoint_text',  '● ' ],
	\ ['debugger_currentline_text', '🢂 ' ],
	\
	\ ['carriage_return_text_unix~F>T', ['unix', '🐧', '']],
	\ ['carriage_return_text_mac~F>T',  ['mac',  '🍎', '']],
	\ ['carriage_return_text_dos~F>T',  ['do$',  '🪟 ', '']],
	\
	\ ['folder_open_status_text_opened~F>T',  ['+',  '▼', '']],
	\ ['folder_open_status_text_closed~F>T',  ['-',  '▶', '']],
	\
	\ ['startify_show_projects', 1],
	\ ['startify_show_projects_at_the_top', 1],
	\ ['startify_sort_projects_by', 'date']
\]

for defaultConfiguration in s:defaultConfigurations
	call s:setDefaultConfiguration ( defaultConfiguration[0], defaultConfiguration[1] )
endfor

let s:configurationProvider = v:null
let s:localConfigurationProvider = v:null

function s:isEnabled(feature)
	let name = "g:".feature
	if exists(name && eval(name) != 0)
		return 1
	endif
	return 0
endfunction

function ediyalam#init()
	if s:initialized
		return
	endif
	let s:initialized = 1

	au ColorScheme * call ediyalam#color#createHighlights()

	let s:configurationProvider = ediyalam#util#configuration#newConfigurationProvider(s:getUserConfigDirectory())
	let s:localConfigurationProvider = ediyalam#util#configuration#newConfigurationProvider(s:getUserLocalDataDirectory())

	if exists('ediyalam_visual_on_start')
		call ediyalam#visual#Init()
	endif

	call ediyalam#tiers#nerdTree#main#init()
	call ediyalam#tiers#vebugger#main#Init()
	call ediyalam#tiers#git#main#init()
	call ediyalam#tiers#fzf#main#Init()
	call ediyalam#tiers#airline#main#Init()
	call ediyalam#tiers#quickui#main#Init()
	call ediyalam#tiers#startify#main#Init()
	call ediyalam#tiers#cmake#main#init()


	au VimEnter * call ediyalam#color#createHighlights()
	au VimEnter * call ediyalam#workspace#init()


	" au VimLeavePre * call ediyalam#leave()

	let s:projects = s:localConfigurationProvider.getOr('project', s:projects_info_default)
endfunction

function ediyalam#leave()
	call ediyalam#workspace#leave()
endfunction


function ediyalam#registerPlugin(name)
	return ediyalam#plugin#plugin#createBroker(a:name)
endfunction


function s:getUserConfigDirectory()
	return expand('~').'/.config/ediyalam' 
endfunction
function s:getUserLocalDataDirectory()
	return expand('~').'/.local/share/ediyalam' 
endfunction

function! ediyalam#allProjects()
	return s:projects.known
endfunction

function! ediyalam#projectCreateAndAdd ( name, root_directory )
	if ediyalam#project#directoryContainsAProject(a:root_directory)
		call ediyalam#util#system#messageError('A Project already exists in "'.a:root_directory.'"')
		return 0
	endif
	let newProject = ediyalam#project#newProject(a:name,a:root_directory)
	try
		call newProject.save()
	catch
		:redraw!
		call ediyalam#util#system#messageError('Failed to save Project in "'.a:root_directory.'" : '.v:exception)
		return 0
	endtry
	:redraw!
	call ediyalam#util#system#messageDone('Project "' . l:newProject.__p.name .'" created in "' . l:newProject.__rt.path . '"')
	return l:newProject
endfunction
function! ediyalam#projectCreateAroundBufferFileAndAdd ( name )
	return ediyalam#projectCreateAndAdd(a:name,expand("%:p:h"))
endfunction
function! ediyalam#projectCreateInCWDAndAdd ( name )
	return ediyalam#projectCreateAndAdd(a:name,getcwd())
endfunction

function ediyalam#projectOpenAndAddToList(path)
	let project = ediyalam#projectOpen(a:path)
	call s:updateProjectsListAndSaveIt(project)
endfunction
function! ediyalam#projectOpen(path)
	if ! ediyalam#project#directoryContainsAProject(a:path)
		call ediyalam#util#system#messageError("No project in '".a:path."'")
		return
	endif
	let project = ediyalam#project#openProject(a:path)
	let g:ediyalam_project = project
	return project
endfunction
function! ediyalam#projectOpenFromCWD()
	return ediyalam#projectOpen(getcwd())
endfunction
function! ediyalam#projectOpenFromCWDAndAddToList()
	return ediyalam#projectOpenAndAddToList(getcwd())
endfunction
function! ediyalam#projectOpenAroundBufferFile()
	return ediyalam#projectOpen(expand("%:p:h"))
endfunction
function! ediyalam#projectOpenAroundBufferFileAndAddToList()
	return ediyalam#projectOpenAndAddToList(expand("%:p:h"))
endfunction

function! s:updateProjectsListAndSaveIt(project)
	let reference = {}
	let reference.name = a:project.__p.name
	let reference.path = a:project.__rt.path
	let reference.accessed = exists("*strftime") ? strftime("%Y-%m-%d_%H:%M:%S") : 'unknown'
	let replaced = 0
	let index = 0
	for p in s:projects.known
		if p.path ==# reference.path
			let s:projects.known[index] = l:reference
			let replaced = 1
			break
		endif
		let index = index + 1
	endfor
	if ! replaced
		call add(s:projects.known,l:reference)
	endif
	call s:saveProjectsList()
endfunction	

function! s:saveProjectsList() abort
	call s:localConfigurationProvider.put('project', s:projects)
endfunction	


function! ediyalam#pickProjectUp()
	call fzf#run(fzf#wrap({'source': s:projects.known}))
endfunction


function! ediyalam#projectSave(project)
	let index = ediyalam#projectGetIndex(a:project)
	if index < 0
		return 0
	endif

	try
		return a:project.save()
	catch
		:redraw!
		call ediyalam#util#system#messageError('Unable to save Project "'. a:project.root .'" : '.v:exception)
		return
	endtry

	:redraw!
	call ediyalam#util#system#messageDone('Succesfully saved "'.(a:project.name).' in "'.(a:project.root).'"')
endfunction
function! ediyalam#projectSaveCurrent()
	if ! s:CheckProjectionSelection() | return 0 | endif
	return ediyalam#projectSave(ediyalam#ProjectGetCurrent())
endfunction

function! ediyalam#projectRename(project,name)
	let index = ediyalam#projectGetIndex(a:project)
	if index < 0
		return 0
	endif
	let old_name = a:project.name
	let a:project.name = a:name
	call ediyalam#util#system#messageDone('Project "'.old_name.'" is now known as "'.a:name.'"')
	return 1
endfunction
function! ediyalam#projectRenameCurrent(name)
	if ! s:CheckProjectionSelection() | return 0 | endif
	return ediyalam#projectRename(ediyalam#ProjectGetCurrent(),a:name)
endfunction



function! ediyalam#projectClose(project)
	call ediyalam#project#closeProject(a:project)
endfunction
function! ediyalam#projectCloseCurrent()
	if g:ediyalam_project is v:null
		call ediyalam#util#log#error("Not opened project")
	endif
	return ediyalam#projectClose(g:ediyalam_project)
endfunction

function! ediyalam#projectDelete(project)
	try
		if ! ediyalam#projectClose (a:project)
			call ediyalam#util#system#messageError('Project "'.a:project.name.'" could not be closed".')
			return 0
		endif
		let project_name = a:project.name
		let project_root = a:project.root
		call a:project.erase()
	catch
		:redraw!
		call ediyalam#util#system#messageError('Unable to delete Project "'.a:project.name.'" : '.v:exception)
		return 0
	endtry
	:redraw!
	call ediyalam#util#system#messageDone('Project "'.project_name.'" in "'.project_root.'" has been deleted.')
	return 1
endfunction
function! ediyalam#projectDeleteCurrent()
	if ! s:CheckProjectionSelection() | return 0 | endif
	return ediyalam#projectDelete(ediyalam#ProjectGetCurrent())
endfunction

function! ediyalam#projectAddPlugin(project, pluginName) abort
	let plugin = ediyalam#plugin#plugin#getBrokerOrThrow(a:pluginName)
	call a:project.applyPlugin(plugin)
	call a:project.configurePlugin(plugin.name)
endfunction

function! s:BuildInfoLinePadded(project,width_projects_name)
	let flag = ' '
	let index = ediyalam#projectGetIndex(a:project)
	let flag_current = ' '
	if index == s:current_project_index
		let flag_current = '*'
	endif
	if a:project.isOpen()
		let flag = 'O'
	else
		let flag = 'C'
	endif
	let project_name = a:project.name
	if a:width_projects_name > 0
		let project_name = ediyalam#util#Pad('"'.a:project.name.'"',2+a:width_projects_name)
	endif
	return a:project.id.'('.flag.') '.flag_current.' '.project_name.' at "'.(a:project.root).'"'
endfunction

function! s:BuildInfoLine(project)
	return BuildInfoLinePadded(a:project,0)
endfunction

function! ediyalam#listProjects (openedOnly)
	let result = []
	for project in s:projects.known
		if a:openedOnly && ! project.isOpen()
			continue
		endif
		call add ( result, project )
	endfor
	return result
endfunction
function! ediyalam#projectShow(openedOnly)
	let projectsTrimed = ediyalam#listProjects(a:openedOnly)
	if len(projectsTrimed) > 0
		let width_projects_name=0
		for project in projectsTrimed
			let width=strlen(project.name) | if width>width_projects_name | let width_projects_name=width | endif
		endfor
		echo g:ediyalam_message_prefix . 'Project(s) :'
		for project in projectsTrimed
			echo s:BuildInfoLinePadded(project,width_projects_name)
		endfor
	else
		echo g:ediyalam_message_prefix . 'No Project'
	endif
endfunction
function! ediyalam#projectShowAsText(openedOnly)
	let projectsTrimed = ediyalam#listProjects(a:openedOnly)
	let result = []
	for project in projectsTrimed
		call add ( result, s:BuildInfoLine(project) )
	endfor
	echo result
endfunction


