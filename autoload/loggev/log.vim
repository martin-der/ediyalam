
if exists('g:loggev_done')
	finish
end
let g:loggev_done=1


function! s:default_plugin_log_debug(log, level, text)
	echohl Normal | echom '[D] '.a:log.prefix . a:text | echohl None
endfunction
function! s:default_plugin_log_info(log, level, text)
	echohl Normal | echom '[I] '.a:log.prefix . a:text | echohl None
endfunction
function! s:default_plugin_log_warn(log, level, text)
	echohl Error | echom '[W] '.a:log.prefix . a:text | echohl None
endfunction
function! s:default_plugin_log_error(log, level, text)
	echohl Error | echom '[E] '.a:log.prefix . a:text | echohl None
endfunction
let s:default_fct_ref = {
	\ 'fct_debug': function('s:default_plugin_log_debug'),
	\ 'fct_info': function('s:default_plugin_log_info'),
	\ 'fct_warn': function('s:default_plugin_log_warn'),
	\ 'fct_error': function('s:default_plugin_log_error')
\}

let s:Log_Level = { 'all':5, 'debug':4, 'info':3, 'warn':2, 'error':1, 'none': 0 }
let s:Log = {}
let s:Log.__log = {}
function s:Log.setLevel(level) dict
	let t = type(a:level)
	if t == v:t_string
		if a:level ==? 'all'                               | let self.level = s:Log_Level.all   | let self.__public.level = self.level | return | endif
		if a:level ==? 'debug'                             | let self.level = s:Log_Level.debug | let self.__public.level = self.level | return | endif
		if a:level ==? 'info' || a:level ==? 'information' | let self.level = s:Log_Level.info  | let self.__public.level = self.level | return | endif
		if a:level ==? 'warn' || a:level ==? 'warning'     | let self.level = s:Log_Level.warn  | let self.__public.level = self.level | return | endif
		if a:level ==? 'error'                             | let self.level = s:Log_Level.error | let self.__public.level = self.level | return | endif
		if a:level ==? 'none'                              | let self.level = s:Log_Level.none  | let self.__public.level = self.level | return | endif
	elseif t == v:t_number
		if a:level >= s:Log_Level.all
			let self.level = s:Log_Level.all
		elseif a:level <= s:Log_Level.none
			let self.level = s:Log_Level.none
		else
			let self.level = a:level
		endif
		let self.__public.level = self.level
		return
	endif
	throw "Not a valid log level : '".a:level."'"
endfunction
function s:Log.debug(text) dict
	if self.level >= s:Log_Level.debug
		call self.__log.debug(self.__public, s:Log_Level.debug, a:text)
	endif
endfunction
function s:Log.info(text) dict
	if self.level >= s:Log_Level.info
		call self.__log.info(self.__public, s:Log_Level.info, a:text)
	endif
endfunction
function s:Log.warn(text) dict
	if self.level >= s:Log_Level.warn
		call self.__log.warn(self.__public, s:Log_Level.warn, a:text)
	endif
endfunction
function s:Log.error(text) dict
	if self.level >= s:Log_Level.error
		call self.__log.error(self.__public, s:Log_Level.error, a:text)
	endif
endfunction


function loggev#log#newPlugin(prefix, level)
	let l:log = copy(s:Log)
	if a:prefix is v:null
		let l:log.prefix = ''
	else
		let l:log.prefix = a:prefix
	endif

	let l:log.__public = {}
	let l:log.__public.prefix = l:log.prefix

	call l:log.setLevel(a:level)

	let l:log.__log.debug = s:default_fct_ref.fct_debug
	let l:log.__log.info = s:default_fct_ref.fct_info
	let l:log.__log.warn = s:default_fct_ref.fct_warn
	let l:log.__log.error = s:default_fct_ref.fct_error

	return l:log
endfunction


