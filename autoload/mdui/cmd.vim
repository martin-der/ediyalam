

let s:CmdController = {}

" config = {
"     ui : {
"         cmd {
"             height : height of the command panel
"                 if not provided height will be an educated guess will be made : 
"                   1 
"                   + 1 if a header if provided 
"                   + 1 if a validate function provided
"         }
"     }

function s:setCmdheight(config) abort

	if has_key(a:config.ui, 'cmd')
		let cmd = a:config.ui.cmd
		if has_key(cmd, 'height')
			let &cmdheight = cmd.œheight
			return
		endif
	endif

	" Try to guess height
	let l:height = 1  
	if has_key(a:config.ui, 'header')
		let header = a:config.ui.header
		if has_key(header, 'isParagrah') && header.isParagrah
			let l:height += 1
		endif
	endif
	if has_key(a:config, 'validation')
		let l:height += 1
	endif
	let &cmdheight = l:height
endfunction


function s:saveStateAndPrepare(controller, state)
    let a:state.oldLazyredraw = &lazyredraw
    let a:state.oldCmdheight = &cmdheight
	" let a:state.oldGuicursor = &guicursor
	" n-v-c-sm:block
    set nolazyredraw
    call s:setCmdheight(a:controller._config)
endfunction

function s:restoreState(controller, state)
    let &cmdheight = a:state.oldCmdheight
    let &lazyredraw = a:state.oldLazyredraw
    " let &guicursor = a:state.oldGuicursor
endfunction

function mdui#cmd#newStringController(config)
	let controller = mdui#common#newStringController(a:config)
	call extend(controller, s:CmdController)
	call controller.setStateHandlers(function('s:saveStateAndPrepare'), function('s:restoreState'))
	return controller
endfunction

function mdui#cmd#newSelectOneController(config)
	let controller = mdui#common#newSelectOneController(a:config)
	call extend(controller, s:CmdController)
	call controller.setStateHandlers(function('s:saveStateAndPrepare'), function('s:restoreState'))
	return controller
endfunction

