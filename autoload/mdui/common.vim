
let s:BaseController = {  '_state': {}, '_helper': {}, '_messages': [] }

function mdui#common#newBaseController(config)
	let controller = deepcopy(s:BaseController)
	let config_all = deepcopy(s:configTemplate)
	call extend(config_all, a:config)
	let controller._config = config_all
	call s:initBase(controller, controller._config)
	return controller
endfunction

let s:configTemplate = {'ui': {}}

function s:initBase(controller, config)
	if has_key(a:config, 'validation')
		let validation = a:config.validation
		if has_key(validation, 'agent')
			let a:controller._helper.validation = s:newValidationHelper(a:controller)
		endif
	endif
endfunction

function s:BaseController.setStateHandlers(saveAndPrepare, restore) abort
	if type(a:saveAndPrepare) != v:t_func
		throw "BaseController.setStateHandlers argument 'saveAndPrepare' must be a function"
	endif
	if type(a:restore) != v:t_func
		throw "BaseController.setStateHandlers argument 'restore' must be a function"
	endif
	let self._state.saveAndPrepare = a:saveAndPrepare
	let self._state.restore = a:restore
endfunction

" ░▒▓█


let s:Controller = {}
let s:Controller._ui = { 'endLineCursor': '▉'}

" Controller has the abitility to make its custom UI
function mdui#common#newController(config)
	let controller = mdui#common#newBaseController(a:config)
	call extend(controller, copy(s:Controller))
	call s:_init(controller, controller._config)
	return controller
endfunction


function s:_init(controller, config)
	if has_key(a:config, 'completion')
		if ! (has_key(a:config, 'keyChar') || has_key(a:config, 'keyNr') )
			let a:config.completion.keyNr = 9
		endif
	endif
endfunction

function s:Controller.drawHeader() abort
	let l:ui = self._config.ui
	if has_key(l:ui, 'header')
		let header = l:ui.header 
		if has_key(header, 'text')
			echon header.text
		endif
		if has_key(header, 'isParagrah') && header.isParagrah
			echo ''
		endif
	endif
endfunction
function s:Controller.drawFooter()
endfunction

let s:ValidationHelper = {}
function s:newValidationHelper(controller)
	let helper = deepcopy(s:ValidationHelper)
	let helper.controller = a:controller
	return helper
endfunction
function s:ValidationHelper._add(severity, text) dict
	call add(self.controller._messages, { 'severity': a:severity, 'content': a:text })
endfunction
function s:ValidationHelper.addWarn(text) dict
	call self._add('warn', a:text)
endfunction
function s:ValidationHelper.addError(text) dict
	call self._add('error', a:text)
endfunction

function s:Controller.drawValidationMessage(message)
	if a:message.severity ==# 'warn'
		echohl EdiyalamMessageWarn 
	elseif a:message.severity ==# 'error'
		echohl EdiyalamMessageError
	else
		echohl None
	endif
	echon a:message.content
	echohl None
endfunction

function s:Controller.run() dict abort
	if has_key(self._state, 'saveAndPrepare')
		call self._state.saveAndPrepare(self,self._config)
	endif

	try
		let l:done = 0
		let l:status = { }

		while !l:done
			if has('nvim')
				mode
			else
				redraw!
			endif

			let self._messages = []
			if has_key(self._helper, 'validation')
				call self._config.validation.agent(self.getContent(), self._helper.validation, v:null)
			endif

			call self.drawHeader()
			call self.drawContent()
			call self.drawFooter()

			let l:charnr = getchar()
			let l:key = nr2char(l:charnr)
			let l:done = self._handleKeypress(l:key, l:charnr)

		endwhile
	finally
		if has_key(self._state, 'restore')
			call self._state.restore(self,self._config)
		endif

		" Redraw when Ctrl-C or Esc is received.
		if l:done < 2
			redraw!
			return v:null
		endif
	endtry
	return self.doDefaultAction()
endfunction

function s:Controller.doDefaultAction() dict
	return v:null
endfunction

function! s:Controller.validate(validator)
endfunction
function! s:Controller.addMessage(content, severity)
	call add(self._messages, {'content': a:content, 'severity': a:severity})
endfunction


function s:Controller._handleKeypress(key,charnr) dict
	if a:key ==# nr2char(27) "escape
		let self.selection = -1
		return 1
	endif
	" if a:key == "\<Left>
	" 	call _handlePaste()
	" endif
	" if a:key == <80><fd>/
	" 	call _handlePaste()
	" endif
	if has_key(self._config, 'completion')
		let completion = self._config.completion
		if has_key(completion, 'keyChar')
			if a:key is# completion.keyChar
				if has_key(completion, 'fctRef') && type(completion.fctRef) == v:t_function
					let newContent = completion.fctRef(self.getContent(), get(completion,'data',v:null))
					call self.setContent(newContent)
				endif
				return 1
			endif
		elseif has_key(completion, 'keyNr')
			if a:charnr == completion.keyNr
				if has_key(completion, 'fctRef') && type(completion.fctRef) == v:t_function
					let newContent = completion.fctRef(self.getContent(), get(completion,'data',v:null))
					call self.setContent(newContent)
				endif
				return 1
			endif
		endif
	endif
	if a:key ==# "\r" || a:key ==# "\n" "enter and ctrl-j
		return 2
	endif

	call self.handleKeypress(a:key,a:charnr)

	return 0
endfunction

" config = {
"     content : ... " Implémentation spécific 
"     ui : {
"         header : {
"             isParagrah : boolean[default=false]
"             text       : string
"         }
"     }
"     drawHeader : function reference
"     drawFooter : function reference
"     completion : {
"         keyChar : the char that trigger the completion, defaults to 'tab'
"         keyNr   : the key number that trigger the completion, defaults to 'tab'
"         agent(???, helper, data) : function that performs the completion
"     }
"     validation : {
"         agent(text, helper, data) : function that performs the validation
"                @param text : string
"                @param helper : object with 'addError', 'addWarn' functions
"                @param data : user supplied data
"     }
" }

" Child Controller override
"     handleKeypress(keyChar,keyNr)
"     drawContent()
"     drawFooter()
"     doDefaultAction(content)

" *** ----------------- ***
" *** String Controller ***
" *** ----------------- ***

" config = {
"     content : { value : String }
" }

let s:StringController = {}

let s:StringController.content = ''
function s:StringController.getContent() dict
	return self.content
endfunction
function s:StringController.setContent(content) dict
	let self.content = a:content
endfunction
function s:StringController.handleKeypress(key,charnr) dict
	if a:charnr is# "\<BS>" "backspace
		if len(self.content)>0
			let self.content = self.content[0:-2]
		endif
		return 1
	endif
	let self.content = self.content . a:key
	return 1
endfunction
function s:StringController.drawContent() dict
	echon self.content . self._ui.endLineCursor
endfunction
function s:StringController.drawFooter() dict
	echo ''
	if len(self._messages)>0
		call self.drawValidationMessage(self._messages[0])
	endif
endfunction
function s:StringController.doDefaultAction() dict
	return self.content
endfunction



function s:newStringController(config)
	let controller = mdui#common#newController(a:config)
	call extend(controller, copy(s:StringController))
	if has_key(a:config, 'content') && has_key(a:config.content, 'value')
		call controller.setContent(a:config.content.value)
	endif
	return controller
endfunction

function mdui#common#newStringController(config)
	return s:newStringController(a:config)
endfunction


" *** -------------------- ***
" *** SelectOne Controller ***
" *** -------------------- ***

" config = {
"     content : { selection : Number, items : [] }
"     ui : {
"         drawItem : function(item, state, config, data)
"               @param item
"               @param state : { selected: 1/0 }
"               @param config
"               @param data
"     }
" }
let s:SelectOneController = {}
let s:SelectOneController.content = { 'index':-1, 'items': [] }

function s:SelectOneController.getContent() dict
	return self.content
endfunction
function s:SelectOneController.setContent(content) dict
	let self.content = a:content
endfunction
function s:SelectOneController.handleKeypress(key,keyNr) dict
	let countItems = len(self.content.items)
	
	if ! countItems
		let self.content.index = -1
		return 1
	endif

	if a:keyNr == "\<Down>"
		if countItems < 0
			let self.content.index = 0
		else
			let self.content.index = self.content.index + 1
			if self.content.index >= countItems
				let self.content.index = 0
			endif
		endif
		return 1
	endif
	if a:keyNr == "\<Up>"
		if self.content.index < 0
			let self.content.index = countItems-1
		elseif self.content.index >= countItems
			let self.content.index = countItems-1
		else
			let self.content.index = self.content.index - 1
			if self.content.index < 0
				let self.content.index = countItems-1
			endif
		endif
		return 1
	endif

	return 0
endfunction
function s:SelectOneController.drawContent() dict
	let index = 0
	let itemsLen = len(self.content.items)
	for item in self.content.items
		let selected = index == self.content.index
		if selected
			echon "> "
		else
			echon "  "
		endif
		if has_key(self._config.ui, 'drawItem')
			call self._config.ui.drawItem(item, { 'selected': selected }, self._config, v:null)
		else
			echon item
		endif
		if index<itemsLen-1
			echo ''
		endif
		let index = index+1
	endfor
endfunction
function s:SelectOneController.doDefaultAction() dict
	if self.content.index<0 | return v:null | endif
	return { 'index': self.content.index, 'item': self.content.items[self.content.index] }
endfunction



function s:newSelectOneController(config)
	let controller = mdui#common#newController(a:config)
	call extend(controller, deepcopy(s:SelectOneController))
	let controller.content.items = a:config.content.items
	let controller.content.index = a:config.content.selection
	return controller
endfunction

function mdui#common#newSelectOneController(config)
	return s:newSelectOneController(a:config)
endfunction

