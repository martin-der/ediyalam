#!/usr/bin/env bash

# env var :
# * VIM_EXECUTABLE
# * TEST_PLUGIN_PATH_VADER

set -euo pipefail

cd "$(dirname "${0}")" || exit 1


test x == "x${VIM_EXECUTABLE:-}" && VIM_EXECUTABLE=vim

RTP_VADER="${TEST_PLUGIN_PATH_VADER:-}"
test x == "x${RTP_VADER}" && RTP_VADER="../.ci/build-cache/vader.vim"


VIM_SCRIPT="VIMRC
filetype off
set rtp+=${RTP_VADER}
set rtp+=..
filetype plugin indent on
syntax enable"

"${VIM_EXECUTABLE}" -esNu <( echo "$VIM_SCRIPT" ) -c 'Vader! src/**/*.vader'
# "${VIM_EXECUTABLE}" -Nu <( echo "$VIM_SCRIPT" ) -c 'Vader! src/**/*.vader'
echo $?


