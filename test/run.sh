#!/usr/bin/env bash

# env var :
# * VIM_EXECUTABLE
# * TEST_PLUGIN_PATH_VADER
# * TEST_PLUGIN_PATH_HIH
# * TEST_PLUGIN_PATH_VMOCK

set -euo pipefail

cd "$(dirname "${0}")" || exit 1
HERE="$(pwd)"


test x == "x${VIM_EXECUTABLE:-}" && VIM_EXECUTABLE=vim

RTP_HIH="${TEST_PLUGIN_PATH_HIH:-}"
test x == "x${RTP_HIH}" && RTP_HIH="${HERE}/../.ci/build-cache/plugin/hih"
RTP_VADER="${TEST_PLUGIN_PATH_VADER:-}"
test x == "x${RTP_VADER}" && RTP_VADER="${HERE}/../.ci/build-cache/plugin/vader.vim"
RTP_VMOCK="${TEST_PLUGIN_PATH_VMOCK:-}"
test x == "x${RTP_VMOCK}" && RTP_VMOCK="${HERE}/../.ci/build-cache/plugin/vmock"

VIM_SCRIPT="
filetype off
set rtp=${RTP_VADER}
set rtp+=${RTP_HIH}
set rtp+=${RTP_VMOCK}
set rtp+=${HERE}/..
filetype plugin indent on
syntax enable"

result=0
"${VIM_EXECUTABLE}" -esNu <( echo "$VIM_SCRIPT" ) -c 'Vader! src/**/*.vader' && result=1

test ${result} -eq 0 && exit 1
exit 0
