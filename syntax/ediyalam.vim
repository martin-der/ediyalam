" File:        ediyalam.vim
" Description: Ediyalam syntax settings
" Author:      
" Licence:     Vim licence
" Website:     
" Version:     0.0.1

scriptencoding utf-8

if exists("b:current_syntax")
    finish
endif

let s:ics = escape('+-', ']^\-')
let s:pattern = '\(^[' . s:ics . '] \?\)\@<=[^-+: ]\+[^:]\+$'
execute "syntax match EdiyalamKind '" . s:pattern . "'"

let s:pattern = '\(\S\@<![' . s:ics . '][-+# ]\?\)\@<=[^*(]\+\(\*\?\(([^)]\+)\)\? :\)\@='
execute "syntax match EdiyalamScope '" . s:pattern . "'"

let s:pattern = '\S\@<![' . s:ics . ']\([-+# ]\?\)\@='
execute "syntax match EdiyalamFoldIcon '" . s:pattern . "'"

let s:pattern = '\(\S\@<![' . s:ics . ' ]\)\@<=+\([^-+# ]\)\@='
execute "syntax match EdiyalamVisibilityPublic '" . s:pattern . "'"
let s:pattern = '\(\S\@<![' . s:ics . ' ]\)\@<=#\([^-+# ]\)\@='
execute "syntax match EdiyalamVisibilityProtected '" . s:pattern . "'"
let s:pattern = '\(\S\@<![' . s:ics . ' ]\)\@<=-\([^-+# ]\)\@='
execute "syntax match EdiyalamVisibilityPrivate '" . s:pattern . "'"

unlet s:pattern

syntax match EdiyalamHelp      '^".*' contains=EdiyalamHelpKey,EdiyalamHelpTitle
syntax match EdiyalamHelpKey   '" \zs.*\ze:' contained
syntax match EdiyalamHelpTitle '" \zs-\+ \w\+ -\+' contained

syntax match EdiyalamNestedKind '^\s\+\[[^]]\+\]$'
syntax match EdiyalamType       ' : \zs.*'
syntax match EdiyalamSignature  '(.*)'
syntax match EdiyalamPseudoID   '\*\ze :'

highlight default link EdiyalamHelp       Comment
highlight default link EdiyalamHelpKey    Identifier
highlight default link EdiyalamHelpTitle  PreProc
highlight default link EdiyalamKind       Identifier
highlight default link EdiyalamNestedKind EdiyalamKind
highlight default link EdiyalamScope      Title
highlight default link EdiyalamType       Type
highlight default link EdiyalamSignature  SpecialKey
highlight default link EdiyalamPseudoID   NonText
highlight default link EdiyalamFoldIcon   Statement
highlight default link EdiyalamHighlight  Search

highlight default EdiyalamAccessPublic    guifg=Green ctermfg=Green
highlight default EdiyalamAccessProtected guifg=Blue  ctermfg=Blue
highlight default EdiyalamAccessPrivate   guifg=Red   ctermfg=Red
highlight default link EdiyalamVisibilityPublic    EdiyalamAccessPublic
highlight default link EdiyalamVisibilityProtected EdiyalamAccessProtected
highlight default link EdiyalamVisibilityPrivate   EdiyalamAccessPrivate

let b:current_syntax = "ediyalam"

" vim: ts=8 sw=4 sts=4 et foldenable foldmethod=marker foldcolumn=1
