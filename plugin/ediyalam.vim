" ============================================================================
" File:        ediyalam.vim
" Description: Provide utils for Projects manipulation
" Author:      
" Licence:     Vim licence
" Website:    
" Version:     0.0.1    
" Note:        
"              
"
" ============================================================================

scriptencoding utf-8


if exists('g:ediyalam_loaded')
    finish
endif

let g:ediyalam_loaded = 1

let g:ediyalam_project = v:null

" Basic init {{{1

if v:version < 700
    echohl ErrorMsg
    echomsg 'Ediyalam: Vim version is too old, Ediyalam requires at least 7.0'
    echohl None
    finish
endif

if !has("python3")
	"ok... that shouldn't be a problem
endif

"if v:version == 700 && !has('patch167')
"    echohl ErrorMsg
"    echom 'Ediyalam: Vim versions lower than 7.0.167 have a bug'
"          \ 'that prevents this version of Ediyalam from working.'
"    echohl None
"    finish
"endif




let s:ediyalam_tamil='எதியாலாம்'
if ! exists('g:ediyalam_message_prefix')
	if ediyalam#util#system#canFancyFont()
		let g:ediyalam_message_prefix = s:ediyalam_tamil.' | '
	else
		let g:ediyalam_message_prefix = 'Ediyalam | '
	endif
endif




function! s:init_var(var, value) abort
    if !exists('g:ediyalam_' . a:var)
        execute 'let g:ediyalam_' . a:var . ' = ' . string(a:value)
    endif
endfunction

function! s:setup_options() abort
"	if !exists('g:ediyalam_vertical') || g:ediyalam_vertical == 0
"		let previewwin_pos = 'topleft'
"	else
"		let previewwin_pos = 'rightbelow vertical'
"	endif
    let options = [
		\ ['on_enter_load_workspace', 1],
		\ ['on_exit_save_workspace', 1],
		\ ['on_project_focus_cd_into_root', 1],
		\ ['on_project_focus_NERDTree_cd_into_root', 0],
		\
		\ ['unprefixed_commands', 1],
		\
	\ ]

	for [opt, val] in options
		call s:init_var(opt, val)
	endfor
endfunction
call s:setup_options()


function! s:setup_keymaps() abort
    let keymaps = [
        \ ['jump',          '<CR>'],
        \ ['preview',       'p'],
        \ ['previewwin',    'P'],
        \ ['nexttag',       '<C-N>'],
        \ ['prevtag',       '<C-P>'],
        \ ['showproto',     '<Space>'],
        \ ['hidenonpublic', 'v'],
        \
        \ ['openfold',      ['+', '<kPlus>', 'zo']],
        \ ['closefold',     ['-', '<kMinus>', 'zc']],
        \ ['togglefold',    ['o', 'za']],
        \ ['openallfolds',  ['*', '<kMultiply>', 'zR']],
        \ ['closeallfolds', ['=', 'zM']],
        \ ['nextfold',      'zj'],
        \ ['prevfold',      'zk'],
        \
        \ ['togglesort',      's'],
        \ ['toggleautoclose', 'c'],
        \ ['zoomwin',         'x'],
        \ ['close',           'q'],
        \ ['help',            ['<F1>', '?']],
    \ ]

    for [map, key] in keymaps
        call s:init_var('map_' . map, key)
        unlet key
    endfor
endfunction
"call s:setup_keymaps()

"augroup EdiyalamSession
"    autocmd!
"    autocmd SessionLoadPost * nested call ediyalam#RestoreSession()
"augroup END

" Commands {{{1

function! ProjectBuildTypeComplete(arg, line, pos)
	return [ 'cmake', 'maven' ]
endfunction
function! ProjectSelectionComplete(arg, line, pos)
	let isNameCompletion = ( a:arg =~ "^'" )
	let result = []
	echo 'arg:'.a:arg.' line:'.a:line.' pos:'.a:pos
	let projects = ediyalam#listProjects(0)
	for project in projects
		if isNameCompletion
			let nameline = a:arg[1:]
			let projectname = project.name[0:len(nameline)]
			"if projectname == nameline
				call add(result, "'".project.name."'")
			"else 
			"	call add(result, "'nameline = ".nameline." projectname = ".projectname)
			"endif
		else
			call add(result, string(project.id))
		endif
	endfor
	return result
endfunction
function! ProjectBarComplete(arg, line, pos)
	return [ 'this', 'that', 'nothing', 'all' ]
endfunction

" Custom completion function for the command 'Foo'
"    -complete=customlist,FooComplete
function! FooComplete(arg, line, pos)
	let l = split(a:line[:a:pos-1], '\%(\%(\%(^\|[^\\]\)\\\)\@<!\s\)\+', 1)
	let argIndex = len(l) - index(l, 'Foo') - 1

	"let funcs = ['EdiyalamProjectBarComplete', 'EdiyalamProjectBuildTypeComplete']
	"return call(funcs[argIndex], [a:arg, a:line, a:pos])
	if argIndex == 1
		return ProjectBuildTypeComplete(a:arg, a:line, a:pos)
	endif
	return ProjectBarComplete(a:arg, a:line, a:pos)
endfunction


command! -nargs=* -complete=dir
				\ EdiyalamProjectCreateIn		call ediyalam#project#createProject(<args>)
command! -nargs=1 EdiyalamProjectCreate			call ediyalam#project#createProjectInCWD(<args>)
command! -nargs=0 EdiyalamProjectSave			call ediyalam#project#saveProject()
command! -nargs=1 -complete=dir
                \ EdiyalamProjectOpen			call ediyalam#projectOpenAndAddToList(<args>)
command! -nargs=0 EdiyalamProjectClose			call ediyalam#project#closeProject()

command! -nargs=0 EdiyalamHelpShowKeymap		call ediyalam#help#showKeymap()

command! -nargs=0 EdiyalamProjectsList		call ediyalam#projectShow(0)
command! -nargs=0 EdiyalamProjectShow		call ediyalam#projectShowAsText(0)

command! -nargs=1 -complete=customlist,ProjectSelectionComplete
                \ EdiyalamProjectSelect					call ediyalam#projectSelect(<args>)
" command! -nargs=* -complete=customlist,FooComplete 
                " \ EdiyalamProjectCreate					call ediyalam#projectCreateAndAdd(<f-args>)
command! -nargs=1 EdiyalamProjectCreateInCWD			call ediyalam#projectCreateInCWDAndAdd(<args>)
command! -nargs=1 EdiyalamProjectCreateAroundBufferFile	call ediyalam#projectCreateAroundBufferFileAndAdd(<args>)
command! -nargs=0 EdiyalamProjectOpenAroundBufferFile	call ediyalam#projectOpenAroundBufferFileAndAdd()
command! -nargs=0 EdiyalamProjectOpenFromCWD			call ediyalam#projectOpenFromCWD()
command! -nargs=1 EdiyalamProjectRenameCurrent			call ediyalam#projectRenameCurrent(<args>)
command! -nargs=0 EdiyalamProjectSaveCurrent			call ediyalam#projectSaveCurrent()


command! -nargs=0 EdiyalamProjectCloseCurrent			call ediyalam#projectCloseCurrent()	
command! -nargs=0 EdiyalamProjectDeleteCurrent			call ediyalam#projectDeleteCurrent()

command! -nargs=0 EdiyalamFindResource call ediyalam#resource#FindResource()

command! -nargs=0 EdiyalamQuickLaunch call ediyalam#process#QuickLaunch()


command! -nargs=1 EdiyalamSetTextMarkers			call ediyalam#visual#SetTextMarkers(<args>)
command! -nargs=0 EdiyalamToggleTextMarkers			call ediyalam#visual#ToggleTextMarkers()
command! -nargs=0 EdiyalamToggleLineNumbers			call ediyalam#visual#ToggleLineNumbers()
command! -nargs=0 EdiyalamToggleGutter				call ediyalam#visual#ToggleGutter()
command! -nargs=0 EdiyalamToggleColorTheme			call ediyalam#visual#ToggleColorTheme()
command! -nargs=0 EdiyalamToggleColorFlavor			call ediyalam#visual#ToggleColorFlavor()
command! -nargs=0 EdiyalamToggleColorThemeAirline	call ediyalam#visual#ToggleColorThemeAirline()
command! -nargs=0 EdiyalamToggleTabWidth			call ediyalam#visual#ToggleTabWidth()
command! -nargs=0 EdiyalamToggleLineWrap			call ediyalam#visual#ToggleLineWrap()
command! -nargs=0 EdiyalamToggleMouse				call ediyalam#visual#ToggleMouse()

command! -nargs=0 EdiyalamNERDTreeTabsGoInOrClose	call ediyalam#tiers#nerdTree#main#goInTabsOrClose()

command! -nargs=+ -complete=file
                \ EdiyalamDebugStartGDB				call ediyalam#debug#debug#StartGDB([<f-args>][0],{'args':[<f-args>][1:]})
command! -nargs=0 EdiyalamDebugKill					call ediyalam#debug#debug#KillGDB()
command! -nargs=0 EdiyalamDebugStepOver				call ediyalam#debug#debug#StepOver()
command! -nargs=0 EdiyalamDebugStepIn				call ediyalam#debug#debug#StepIn()
command! -nargs=0 EdiyalamDebugStepOut				call ediyalam#debug#debug#StepOut()
command! -nargs=0 EdiyalamDebugStepContinue			call ediyalam#debug#debug#Continue()

command! -nargs=0 EdiyalamDebugToggleBreakpointThisLine	call ediyalam#debug#debug#ToggleBreakpointThisLine()

command! -range -nargs=0 EdiyalamDebugEvalSelected		call ediyalam#debug#debug#evaluateSelected()
command! -nargs=0 EdiyalamDebugEval		call ediyalam#debug#debug#Evaluate()


"command! -nargs=0 EdiyalamClose             call ediyalam#CloseWindow()
"command! -nargs=1 -bang EdiyalamSetFoldlevel    call ediyalam#SetFoldLevel(<args>, <bang>0)
"command! -nargs=0 EdiyalamShowTag           call ediyalam#highlighttag(1, 1)


" autocmd VimEnter    * call ediyalam#init()
call ediyalam#init()

